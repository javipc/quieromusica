import configparser

class opciones:
    
        
    def __init__ (self, nombre_archivo = 'configuracion.ini', seccion = 'opciones'):        
        self.configuracion = configparser.ConfigParser ()
        self.leer(nombre_archivo)
        if seccion is not None:
            self.sector (seccion)
        

    def leer (self, nombre_archivo = 'configuracion.ini'):
        self.nombre_archivo = nombre_archivo
        self.configuracion.read (self.nombre_archivo)
        
        

    def guardar (self, nombre_archivo = None):
        if nombre_archivo is not None:
            self.nombre_archivo = nombre_archivo        
        with open(self.nombre_archivo, 'w', encoding='utf8') as configfile:
            self.configuracion.write(configfile)


    def sector (self, seccion):
        if seccion not in self.configuracion.sections ():
            self.configuracion.add_section (seccion)
        self.seccion = seccion
        return self

    def establecer (self, parametro = 'activo', valor = None):                                
        self.configuracion.set (self.seccion, parametro, str (valor))
        
    def obtener (self, parametro = 'activo', predeterminado = None):
        if self.existe (parametro):
            valor = self.configuracion.get (self.seccion, parametro)
            valor_l = valor.lower ().strip ()
            if valor_l == 'true':
                return True
            if valor_l == 'false':
                return False
            if valor_l == 'none':
                return None
            if valor_l.isnumeric():
                return int (valor_l)
            return valor


        
        if predeterminado is not None:
            self.establecer (parametro, predeterminado)
            
        return predeterminado

    def predeterminado (self, parametro, valor):
        if not self.existe (parametro):
            self.establecer (parametro, valor)

    def existe (self, parametro):
        if self.seccion not in self.configuracion.sections ():
            return False
        return parametro in self.configuracion.options (self.seccion)

    def tipo (self, parametro):
        return type (self.obtener (parametro))

    def configuracion (self):
        return self.configuracion