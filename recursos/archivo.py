from os import path, makedirs, remove

def leer_archivo (nombre_archivo):
    if not path.exists (nombre_archivo):
        return []
    if not path.isfile (nombre_archivo):
        return []

    try:
        archivo = open(nombre_archivo,'r',encoding='utf8') 
        lista = archivo.readlines ()
        archivo.close ()
    except:
        lista = []    
    lista = [r.replace('\n', '') for r in lista]
    lista = [r.replace('\r', '') for r in lista]
    return lista

def guardar_archivo (nombre_archivo, lista):
    definir_ruta (nombre_archivo)
    with open(nombre_archivo, 'w', encoding='utf8') as archivo:
        for linea in lista:
            archivo.write(str (linea))
            archivo.write('\n')
        archivo.close()



def crear_ruta (ruta = 'archivos/configuracion.ini'):
    directorio = ruta.rpartition ('/') [0]
    if path.isfile (ruta):
        return
    if directorio == '':
        return
    if not path.exists (directorio):
        makedirs(directorio)


def eliminar_archivo (archivo):
    if path.exists(archivo):
        try:
            remove(archivo)
        except:
            print("ERROR al intentar eliminar el archivo: " + archivo )
    else:
        print("El archivo " + archivo + " no existe")


def eliminar_directorio (directorio):
    if path.exists(directorio):
        try:
            os.rmdir(directorio)
        except OSError as e:
            print(f"Error:{ e.strerror}")
    print("El directorio " + directorio + " no existe")