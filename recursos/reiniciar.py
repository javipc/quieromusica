#!/bin/python3

import os
import sys
import __main__
import time

def reiniciar (tiempo = 1):
    time.sleep (tiempo)
    print    ('¡Reiniciando.!')
    print    (sys.executable, [__main__.__file__])  
    os.execl (sys.executable, sys.executable, __main__.__file__)
    print    ('¡Adios!')