from datetime import *



def es_pasado (fecha):
    
    ahora = datetime.utcnow ()
    # print ('fecha: ' + str (fecha) )
    # print ('ahora: ' + str (ahora))
    
    # print ('año',fecha.year , ahora.year)
    if fecha.year > ahora.year:
        return False
    
    # print ('mes', fecha.month , ahora.month)
    if fecha.month > ahora.month:
        return False

    # print ('dia', fecha.day , ahora.day)
    if fecha.day > ahora.day:
        return False

    # print ('hora',fecha.hour , ahora.hour)
    if fecha.hour > ahora.hour:
        return False

    # print ('min', fecha.minute , ahora.minute)
    if fecha.minute > ahora.minute:
        return False

    # print ('seg', fecha.second , ahora.second)
    if fecha.second > ahora.second:
        return False
    return True


# d = datetime (2021, 6, 15, 8, 10, 34, tzinfo=timezone.utc)
