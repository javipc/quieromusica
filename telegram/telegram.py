"""

    Telegram.
    Basado en pyrogram.

    Javier Martinez,  Agosto 2022

"""


from telegram.eventos  import Telegram_Evento
from telegram.mensajes import Telegram_Mensaje


class Telegram (Telegram_Evento, Telegram_Mensaje):

    def __init__ (self):
        super ().__init__ ()
        self.propietario = None
        self.evento_comando (self.ev_ping, ['ping', '🏓'])

    def mi_usuario (self):
        return self.cliente.me.username

    def mi_nombre (self):
        return self.cliente.me.first_name
 
    def mi_foto (self):
        return self.cliente.me.photo
 
    def mi_id (self):
        return self.cliente.me.id
 
 
    # robots no pueden usar esto:
    def actualizar_perfil (self, nuevo_nombre):
        return self.cliente.update_profile(first_name=nuevo_nombre)
 

    async def ev_ping (self, cliente, mensaje):
        return await self.responder (mensaje, '🏓')
 
    
 
    
