#!/bin/python3

"""

   Telegram.
   Basado en pyrogram.

   Manejadores de eventos.

   Javier Martinez,  Agosto 2022

"""


import asyncio
from pyrogram.handlers import MessageHandler , CallbackQueryHandler
from telegram.cliente  import Telegram_Cliente
from pyrogram          import filters

# @cliente.on_message(filters.command("start"))
# async def start_command(client, message):
#     print("This is the /start command")
# 

# @cliente.on_callback_query()
# async def answer(client, callback_query):
#     await callback_query.answer(
#         f"Button contains: '{callback_query.data}'",
#         show_alert=True)


# agrega un nuevo comando.
# /ayuda
# telegram_nuevo_comando (cliente, mostrar_ayuda, "ayuda")
# /ayuda /help
# telegram_nuevo_comando (cliente, mostrar_ayuda, ["ayuda", "help"])


# filtros
def filtro_boton (data):
    async def func (flt, _, query):
        return flt.data == query.data

    # "data" kwarg is accessed with "flt.data" above
    return filters.create(func, data=data)

def filtro_boton_comando (comando):
    async def func (flt, _, query):                
        return flt.data == query.data.split(" ", 1) [0]

    # "data" kwarg is accessed with "flt.data" above
    return filters.create(func, data=comando)




class Telegram_Evento (Telegram_Cliente):

    def evento_comando (self, funcion, comando):
        print ("Activando comando: ", comando)
        self.cliente.add_handler(MessageHandler(callback=funcion, filters=filters.command(comando)))


    def evento_boton (self, funcion, comando):
        print ("Activando boton: ", comando)
        self.cliente.add_handler(CallbackQueryHandler(callback=funcion, filters=filtro_boton(comando)))

    def evento_boton_comando (self, funcion, comando):
        print ("Activando boton: ", comando)
        self.cliente.add_handler(CallbackQueryHandler(callback=funcion, filters=filtro_boton_comando(comando)))

    def evento_texto (self, funcion, filtros=filters.text):
        print ("Activando texto: ")
        self.cliente.add_handler(MessageHandler (callback=funcion, filters=filtros))

    def evento_entrada (self, funcion, filtros=None):
        print ("Activando entrada: ")
        self.cliente.add_handler(ChatJoinRequestHandler (callback=funcion, filters=filtros))
        
        