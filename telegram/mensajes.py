#!/bin/python3

"""

   Telegram.
   Basado en pyrogram.

   Manejadores de mensajes.

   Javier Martinez,  Agosto 2022

"""


import asyncio
from telegram.cliente import Telegram_Cliente
from telegram.botones import Botonera
from pyrogram         import enums
from recursos.msj     import *


class Telegram_Mensaje (Telegram_Cliente):
    

    def enviar_mensaje_sincrono (self, destinatario, texto = "¡Hola!"):
        try:
            return self.cliente.send_message (destinatario, texto)
        except:
            msj ('Ocurrió un error enviando mensaje sincrono a ', destinatario)
            return None

    def borrar_mensaje_sincrono (self, mensaje_original):
        try:
            return mensaje_original.delete ()
        except:
            msj ('Ocurrió un error borrando mensaje sincrono ')
            return None
        

    def obtener_remitente_id (self, mensaje_original):        
        return mensaje_original.from_user.id

    def obtener_sala_id (self, mensaje_original):        
        if hasattr (mensaje_original, 'message'):
            return mensaje_original.message.chat.id
        return mensaje_original.chat.id



    def obtener_texto (self, mensaje):
        return mensaje.text
    def obtener_usuario (self, mensaje):
        return mensaje.from_user.username
    def es_robot (self, mensaje):
        return mensaje.from_user.is_bot
    def esta_verificado (self, mensaje):
        return mensaje.from_user.is_verified
    def es_premium (self, mensaje):
        return mensaje.from_user.is_premium
    def obtener_nombre (self, mensaje):
        return mensaje.from_user.first_name
    def obtener_apellido (self, mensaje):
        return mensaje.from_user.last_name
    def obtener_codigo_idioma (self, mensaje):
        return mensaje.from_user.language_code
    def obtener_id (self, mensaje):
        return mensaje.id





    def obtener_sala_usuario (mensaje):
        return chat.username
    def obtener_sala_tipo (mensaje):
        #ChatType.PRIVATE
        return chat.type 






    async def progreso (self, actual, total, mensaje_progreso=None):
        print(f"{actual * 100 / total:.1f}%")


    async def enviar_accion (self, destinatario, accion = None):
        msj ('Enviando accion a ', destinatario)
        try:
            if accion is None:
                await self.cliente.send_chat_action (destinatario,action=enums.ChatAction.TYPING)
                return
            if accion == 'audio':      
                await self.cliente.send_chat_action (destinatario,action=enums.ChatAction.UPLOAD_AUDIO)
                return
            if accion == 'imagen':
                await self.cliente.send_chat_action (destinatario,action=enums.ChatAction.UPLOAD_PHOTO)
                return
            if accion == 'grabacion':
                await self.cliente.send_chat_action (destinatario,action=enums.ChatAction.RECORD_AUDIO)
                return
            if accion == 'video':
                await self.cliente.send_chat_action (destinatario,action=enums.ChatAction.UPLOAD_VIDEO)
                return
            
            await self.cliente.send_chat_action (destinatario,action=enums.ChatAction.TYPING)
        except:
            msj ('Ocurrió un error enviando acción ', destinatario)



    async def enviar_mensaje (self, destinatario, texto, botones=None):
        msj ('Enviando mensaje a ', destinatario)
        try:
            return await self.cliente.send_message (destinatario, texto, reply_markup=botones )
        except:
            msj ('Ocurrió un error enviando mensaje a ', destinatario)
            return None

    async def enviar_imagen (self, destinatario, texto=None, imagen=None , botones=None, accion=True):
        msj ('Enviando imagen a ', destinatario)
        try:
            if accion:
                await self.enviar_accion (destinatario, 'imagen')
            return await self.cliente.send_photo (destinatario,caption=texto, reply_markup=botones , photo=imagen)
        except:
            msj ('Ocurrió un error enviando mensaje a ', destinatario)
            return None

    async def enviar_audio (self, destinatario, audio, texto=None, botones=None, nombre_archivo=None, accion=True):
        msj ('Enviando audio a ', destinatario)
        if accion:
            await self.enviar_accion (destinatario, 'audio')
        return await self.cliente.send_audio (chat_id=destinatario, audio=audio, caption=texto, file_name=None, reply_markup=botones , progress=self.progreso)

    async def enviar_video (self, destinatario, video, texto=None, botones=None, nombre_archivo=None, accion=True):
        msj ('Enviando video a ', destinatario)
        try:
            if accion:
                await self.enviar_accion (destinatario, 'video')
            return await self.cliente.send_video (chat_id=destinatario, video=video, caption=texto, file_name=None, reply_markup=botones , progress=self.progreso)
        except:
            msj ('Ocurrió un error enviando mensaje a ', destinatario)
            return None

    async def borrar_mensajes (self, destinatario, *ids):
        try:
            return await self.cliente.delete_messages (destinatario, *ids)
        except:
            msj ('Ocurrió un error borrando mensaje a ', destinatario)
            

    async def reenviar_mensaje (self, destinatario, id_sala, id_mensaje):
        msj ('Re enviando a ', destinatario)
        try:
            return await self.cliente.forward_messages (destinatario, id_sala, id_mensaje)
        except:
            msj ('Ocurrió un error reenviando mensaje a ', destinatario)
            return None

    async def copiar_mensaje (self, destinatario, id_sala, id_mensaje, texto = None):
        msj ('Re enviando a ', destinatario)
        try:
            return await self.cliente.copy_message (destinatario, id_sala, id_mensaje, caption=texto)
        except:
            msj ('Ocurrió un error copiando mensaje a ', destinatario)
            return None





    async def responder (self, mensaje_original, texto=None, botones=None):        
        destinatario = self.obtener_sala_id (mensaje_original)
        return await self.enviar_mensaje (destinatario=destinatario, texto=texto, botones=botones)

    async def responder_imagen (self, mensaje_original, imagen , texto=None, botones=None):        
        destinatario = self.obtener_sala_id (mensaje_original)
        return await self.enviar_imagen (destinatario=destinatario, texto=texto, imagen=imagen, botones=botones)

    async def responder_audio (self, mensaje_original, audio, texto=None, nombre_archivo=None, botones=None):
        destinatario = self.obtener_sala_id (mensaje_original)        
        return await self.enviar_audio (destinatario=destinatario, texto=texto, audio=audio, botones=botones, nombre_archivo=None)

    async def responder_video (self, mensaje_original, video, texto=None, nombre_archivo=None, botones=None):
        destinatario = self.obtener_sala_id (mensaje_original)        
        return await self.enviar_video (destinatario=destinatario, texto=texto, video=video, botones=botones, nombre_archivo=None)

    async def responder_accion (self, mensaje_original, accion=None):
        destinatario = self.obtener_sala_id (mensaje_original)
        await self.enviar_accion (destinatario, accion)

    async def responder_reenviar (self, mensaje_original, id_sala, id_mensaje):
        destinatario = self.obtener_sala_id (mensaje_original)        
        return await self.reenviar_mensaje (destinatario=destinatario, id_sala = id_sala, id_mensaje = id_mensaje)

    async def responder_copiar (self, mensaje_original, id_sala, id_mensaje, texto = None):
        destinatario = self.obtener_sala_id (mensaje_original)        
        return await self.copiar_mensaje (destinatario=destinatario, id_sala = id_sala, id_mensaje = id_mensaje, texto = texto)


    async def responder_boton (self, mensaje_original, texto='✅', boton_ok = False):
        try:
            await mensaje_original.answer(texto, show_alert=boton_ok)
        except:            
            msj ('Ocurrió un error respondiendo botón ')            
            return


    async def borrar_mensaje (self, mensaje_original):
        try:
            return await mensaje_original.delete ()
        except:
            msj ('Ocurrió un error borrando mensaje ')
            return None


    def nueva_botonera (self):
        botonera = Botonera ()
        return botonera


    """
    @param mensaje: un objeto que puede contener texto, solicitud del usuario o respuesta de botones
    """
    def obtener_texto (self, mensaje):
        
        if hasattr (mensaje, 'text'):
            return mensaje.text

        if hasattr (mensaje, 'data'):
            return mensaje.data

        # if hasattr (mensaje, 'keys'):
        #     if 'data' in mensaje.keys():
        #         return mensaje.data
        #     if 'text' in mensaje.keys():
        #         return mensaje.text
        
        print ("ERROR - NO HAY TEXTO EN MENSAJE -------")
        print (mensaje)
        print ("---------------------------------------")
        return ''

    def es_comando (self, mensaje):
        texto = self.obtener_texto (mensaje)
        texto=texto.strip()
        if texto.startswith ("/"):
            return True
        return False

    def obtener_comando (self, mensaje):
        texto = self.obtener_texto (mensaje)
        texto=texto.strip()
        if texto.startswith ("/"):
            return texto.split(" ", 1) [0]    
        return None

    def obtener_parametro (self, mensaje):
        texto = self.obtener_texto (mensaje)
        texto=texto.strip()
        comando = self.obtener_comando (mensaje)
        if comando is not None:    
            return texto [len (comando)+1 :] 
        return texto









###########################################






async def demo_enviar_botones (cliente, destinatario):
    reply_markup=InlineKeyboardMarkup(
                [
                    [  # First row
                        InlineKeyboardButton(  # Generates a callback query when pressed
                            "Button",
                            callback_data="data"
                        ),
                        InlineKeyboardButton(  # Opens a web URL
                            "URL",
                            url="https://docs.pyrogram.org"
                        ),
                    ],
                    [  # Second row
                        InlineKeyboardButton(  # Opens the inline interface
                            "Choose chat",
                            switch_inline_query="pyrogram"
                        ),
                        InlineKeyboardButton(  # Opens the inline interface in the current chat
                            "Inline here",
                            switch_inline_query_current_chat="pyrogram"
                        )
                    ]
                ]
            )

    await cliente.send_message (destinatario, "Hola", reply_markup= reply_markup)














# file_id = SetConfigs.tg_user_api.send_audio(
# 			chat_id = bunker_channel,
# 			audio = c_path,
# 			thumb = io_image,
# 			duration = duration,
# 			performer = performer,
# 			title = title,
# 			file_name = file_name
# 		)




#     ### acciones
# 
#     TYPING
# Typing text message
# 
# UPLOAD_PHOTO
# Uploading photo
# 
# RECORD_VIDEO
# Recording video
# 
# UPLOAD_VIDEO
# Uploading video
# 
# RECORD_AUDIO
# Recording audio
# 
# UPLOAD_AUDIO
# Uploading audio
# 
# UPLOAD_DOCUMENT
# Uploading document
# 
# FIND_LOCATION
# Finding location
# 
# RECORD_VIDEO_NOTE
# Recording video note
# 
# UPLOAD_VIDEO_NOTE
# Uploading video note
# 
# PLAYING
# Playing game
# 
# CHOOSE_CONTACT
# Choosing contact
# 
# SPEAKING
# Speaking in group call
# 
# IMPORT_HISTORY
# Importing history
# 
# CHOOSE_STICKER
# Choosing sticker
# 
# CANCEL