#!/bin/python3

"""

   Telegram.
   Basado en pyrogram.

   Conexión.

   Javier Martinez,  Agosto 2022

"""

from pyrogram import Client
from pyrogram import idle as modo_sincrono
from recursos.opciones import opciones





class Telegram_Cliente :

    

    def __init__ (self, nombre_archivo_configuracion = 'configuracion/sesion.ini'):
        configuracion_sesion = opciones (nombre_archivo_configuracion, 'sesion')

        api_id        = configuracion_sesion.obtener ('api_id'        , '')
        api_hash      = configuracion_sesion.obtener ('api_hash'      , '')
        cadena_sesion = configuracion_sesion.obtener ('cadena_sesion' , None)
        cadena_bot    = configuracion_sesion.obtener ('cadena_bot'    , None)
        if cadena_bot is not None:
            self.cliente = Client (name='RoBOT',api_id=api_id, api_hash=api_hash, bot_token=cadena_bot, workers=6)
        else:
            self.cliente = Client (name='RoBOT',api_id=api_id, api_hash=api_hash, session=cadena_sesion, workers=6)

        
        
    def conectar (self):
        if not self.cliente.is_connected:
            self.cliente.start ()

    def desconectar (self):
        if self.cliente.is_connected:
            self.cliente.stop ()

    # esto no funciona en modo asíncrono
    def reiniciar (self):        
        self.cliente.restart ()

    def ejecutar (self, funcion=None):
        if funcion is None:
            self.cliente.run()
        else:            
            self.cliente.run(funcion)


    def ejecutar_sincrono (self):
        self.conectar ()
        modo_sincrono()

    async def ejecutar_sincrono_ (self):        
        await modo_sincrono()