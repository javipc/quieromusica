#!/bin/python3

"""

   Telegram.
   Basado en pyrogram.

   Generador de botones.

   Javier Martinez,  Agosto 2022

"""


#from pyrogram.types import (ReplyKeyboardMarkup, InlineKeyboardMarkup, InlineKeyboardButton)
from pyrogram.types import (InlineKeyboardMarkup, InlineKeyboardButton)




"""
devuelve dos botones con comandos 
boton1: /pagina <pagina anterior> <parametros>
boton2: /pagina <pagina siguiente> <parametros>
@param pagina_actual: la pagina en la que se encuentra
@param parametros: valores adicionales para agregar al comando 
#return: arreglo que sirve como dos botones para usar en una botonera.
"""




#Utilidad para crear botoneras.
class Botonera:
    def __init__ (self):
        self.botones=[]
        self.modo_vertical=True

    """
    @param texto: texto del botón
    @param comando: función que se dispara al pulsar el botón    
    """
    def insertar_vertical (self, texto, comando):        
        self.botones.append ([InlineKeyboardButton (texto, callback_data=comando)])
        self.modo_vertical=True

    """
    @param texto: texto del botón
    @param comando: función que se dispara al pulsar el botón
    """
    def insertar_horizontal (self, texto, comando):        
        if self.modo_vertical or len(self.botones) == 0:
            self.insertar_linea ()
        self.botones[-1].append (InlineKeyboardButton (texto, callback_data=comando))
        self.modo_vertical=False

    """
    inserta una línea vacía para rellenar con botones horizontales
    """
    def insertar_linea (self):
        self.botones.append ([])
        self.modo_vertical=False

    


    # no probado
    def botones_pagina(self, pagina_actual, parametros, limite=None):
        pagina_anterior = f"/pagina {pagina_actual-1} {parametros}"
        pagina_anterior = f"/pagina {pagina_actual+1} {parametros}"

        botones = []
        if pagina_actual > 0:
            botones.append (InlineKeyboardButton("◀️", callback_data=pagina_anterior))

        if limite is None or pagina_actual < limite:
            botones.append (InlineKeyboardButton("▶️", callback_data=pagina_siguiente))

        return botones


    def obtener_botones (self):
        resize_keyboard=True
        if self.botones == [] or self.botones == [[]]:
            return None
        return InlineKeyboardMarkup(self.botones)