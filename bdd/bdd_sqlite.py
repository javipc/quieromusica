#!/usr/bin/python3

"""

	Base de datos SQLite

	Pequeña clase evita código SQL desparramado en el programa.


	Javier Martinez


"""


import sqlite3
from bdd.bdd import Bdd as Bdd_Generico


class Bdd (Bdd_Generico):


	
	"""
	@param base      : Texto, nombre del archivo de base de datos.
	@param usuario   : Texto, No se usa.
	@param clave     : Texto, No se usa.
	@param direccion : Texto, No se usa.
	"""
	def conectar (self, base='mi_base', usuario='javier', clave='hola', direccion='127.0.0.1'):
		
		self.msj  ("Conexión:")
		if self.conexion != None:
			if self.conexion.is_connected ():
				self.msj  ("La base de datos ya está conectada.")
				return

		self.msj  ("Conectando...")
		self.conexion = sqlite3.connect (base + '.bdd')

				
		self.msj  ("Conectado.")
		


	def desconectar (self):
		self.msj  ("Desconectando:")
		if self.conexion != None:
			if self.conexion.is_connected ():
				self.conexion.disconnect ()
				self.msj  ("Desconectado.")
		

	"""
	Consulta genérica :
	@param consulta   : Texto, consulta mysql.
	@param parametros : lista con valores para reemplazar en la cadena de consulta.
	@returns          : cursor.
	"""
	def ejecutar (self, consulta = "SELECT * FROM usuarios WHERE usuario = %s ", parametros = [	]):

		# Convierte consultas en SQLite3
		consulta = consulta.replace ('%s', '?')
		consulta = consulta.replace ('%d', '?')
		consulta = consulta.replace ('AUTO_INCREMENT', 'AUTOINCREMENT')
		consulta = consulta.replace ('AUTO_INCREMENT', 'AUTOINCREMENT')
		consulta = consulta.replace ('AUTOINCREMENT PRIMARY KEY', 'PRIMARY KEY AUTOINCREMENT')
		
		
		bdd_cursor = self.conexion.cursor()
		self.msj  ('Ejecutando: ', consulta, parametros)
		try:
			bdd_cursor.execute(consulta, parametros)
		except Exception as e:
			print ('BDD: ERROR en la consulta ', consulta, ' parámetros: ', parametros)
			if hasattr(e, 'message'):
				print (e.message)
			else:
				print (e)
			return None		
		return bdd_cursor


		