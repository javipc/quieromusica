#!/usr/bin/python3

"""

	Base de datos MySQL / MariaDB

	Pequeña clase evita código SQL desparramado en el programa.


	Javier Martinez


"""


import mysql.connector



from bdd.bdd import Bdd as Bdd_Generico


class Bdd (Bdd_Generico):

	


	
	"""
	@param base      : Texto, nombre de la base de datos que creará y usará al conectarse.
	@param usuario   : Texto, nombre de usuario de la base de datos.
	@param clave     : Texto, contraseña que pertenece al usuario del base de datos.
	@param direccion : Texto, URL de conexión, el servidor de la base de datos.
	"""
	def conectar (self, base='mi_base', usuario='javier', clave='hola', direccion='127.0.0.1'):
		
		self.msj  ("Conexión:")
		if self.conexion != None:
			if self.conexion.is_connected ():
				self.msj  ("La base de datos ya está conectada.")
				return

		self.msj  ("Conectando...")
		self.conexion = mysql.connector.connect (host=direccion, user=usuario, password=clave)

		bdd_cursor = self.conexion.cursor()
		bdd_cursor.execute("CREATE DATABASE IF NOT EXISTS " + base + " DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci")	
		bdd_cursor.execute("USE  " + base)
		self.conexion.commit()
		self.msj  ("Conectado.")
		bdd_cursor.close ()
		bdd_cursor = None


	def desconectar (self):
		self.msj  ("Desconectando:")
		if self.conexion != None:
			if self.conexion.is_connected ():
				self.conexion.disconnect ()
				self.msj  ("Desconectado.")
		

	"""
	Consulta genérica :
	@param consulta   : Texto, consulta mysql.
	@param parametros : lista con valores para reemplazar en la cadena de consulta.
	@returns          : cursor.
	"""
	def ejecutar (self, consulta = "SELECT * FROM usuarios WHERE usuario = %s ", parametros = []):

		if self.conexion.unread_result:
			self.msj  ('LIBERANDO RESULTADOS PENDIENTES ')
			self.conexion.free_result()

		bdd_cursor = self.conexion.cursor()
		self.msj  ('Ejecutando: ', consulta, parametros)
		try:
			bdd_cursor.execute(consulta, parametros)
		except Exception as e:
			print ('BDD: ERROR en la consulta ', consulta, ' parámetros: ', parametros)
			if hasattr(e, 'message'):
				print (e.message)
			else:
				print (e)
			return None		
		return bdd_cursor


