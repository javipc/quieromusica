#!/usr/bin/python3

"""

	Base de datos Genérica

	Pequeña clase evita código SQL desparramado en el programa.


	Javier Martinez


"""





class Bdd:

    def __init__ (self):
        self.modo_prueba = True
        self.conexion = None


    def msj (self, *texto):
        if self.modo_prueba:
            print (' BASE DE DATOS ::: ', *texto)
            

    def obtener_uno (self, cursor):
        if cursor == None:
            return None
        return cursor.fetchone()

    def obtener_todos (self, cursor):
        if cursor == None:
            return None
        return cursor.fetchall()



    """
    cursor = bdd.seleccionar ('usuarios', ['*'], ['chat_id = '],parametros=['11']  )
    cursor = bdd.seleccionar ('usuarios', ['*'] )

    @param tabla       : texto con la tabla (FROM)
    @param columnas    : lista de columnas (SELECT)
    @param condiciones : lista con las columnas que presentarán condiciones acompañado del comparador ('usuario = ')
    @param parametros  : lista o tuplas con parámetros que serán los valores de las condiciones.
    @returns           : cursor.
    """

    def seleccionar (self, tabla= 'usuarios', columnas = ['*'], condiciones = None , parametros = [], limite = '100', orden = None):
        consulta = 'SELECT ' + ', '.join (columnas) + f" FROM {tabla} "
        if condiciones is not None:
            consulta = consulta + " WHERE " + ' %s AND '.join (condiciones) + ' %s'
        if limite is not None:
            consulta = consulta + ' LIMIT ' + str (limite)
        cursor = self.ejecutar (consulta, parametros)
        if str (limite) == '1':			
            return self.obtener_uno (cursor)
        return self.obtener_todos (cursor)

    def seleccionar_uno (self, tabla= 'usuarios', columnas = ['*'], condiciones = None , parametros = []):
        consulta = 'SELECT ' + ', '.join (columnas) + f" FROM {tabla} "
        if condiciones is not None:
            consulta = consulta + " WHERE " + ' %s AND '.join (condiciones) + ' %s'
        cursor = self.ejecutar (consulta, parametros)
        return self.obtener_uno (cursor)


    """
    bdd.insertar ('usuarios', ('chat_id', 'idioma'), ('4592', 'ar'))

    @param tabla       : texto con la tabla (INTO)
    @param columnas    : lista de columnas que tendrán valores para insertar en la tabla.
    @param parametros  : lista o tuplas con parámetros que serán los valores a insertar en la tabla.
    @returns           : cursor.
    """
    def insertar (self, tabla= 'usuarios', columnas = ['usuario'], parametros = ('javi',)):
        consulta = f'INSERT INTO {tabla} (' + ', '.join (columnas) + ') values (' + '%s,' * (len (parametros)-1) +   ' %s )'
        cursor = self.ejecutar (consulta, parametros)
        self.conexion.commit()
        return cursor



    """
    bdd.actualizar ('usuarios', ['idioma', 'formato'], ['br', 'deezer'], ['chat_id =', 'idioma='], ['4591', 'es'] )
    @param tabla                : texto con la tabla (UPDATE)
    @param columnas             : lista de columnas que serán actualizadas (SET)
    @param parametros           : lista con los nuevos valores actualizados.
    @param columnas_condiciones : lista con las columnas que presentarán condiciones acompañado del comparador ('id =').
    @parametros_condiciones     : lista con los parámetros de condiciones son los valores.
    @returns                    : cursor.
    """
    def actualizar (self, tabla= 'usuarios', columnas = ['usuario'], parametros = ('javi',) , condiciones=None, parametros_condiciones=[]):
        consulta = f'UPDATE {tabla} SET ' +  ' = %s , '.join (columnas) + ' = %s'
        if condiciones is not None:
            consulta = consulta + " WHERE " + ' %s AND '.join (condiciones) + ' %s'
        cursor = self.ejecutar (consulta, parametros + parametros_condiciones)
        self.conexion.commit()
        return cursor



    """
    cursor = bdd.insertar_actualizar ('usuarios', ['chat_id','idioma', 'formato'], ['1010','ru', 'deezer'] )

    @param tabla       : texto con la tabla (INTO)
    @param columnas    : lista de columnas que tendrán datos para insertar.
    @param parametros  : lista o tuplas con parámetros que serán los valores a insertar en la tabla.
    @returns           : cursor.
    """

    def insertar_actualizar (self, tabla= 'usuarios', columnas = ['usuario'], parametros = ('javi',)):
        consulta = f'INSERT INTO {tabla} (' + ', '.join (columnas) + ') values (' + '%s,' * (len (parametros)-1) +   ' %s ) ON DUPLICATE KEY UPDATE ' +  ' = %s , '.join (columnas) + ' = %s'
        cursor = self.ejecutar (consulta, parametros + parametros)
        self.conexion.commit()
        return cursor


    """	
    cursor = bdd.eliminar ('usuarios', ['id ='], ['8'])

    @param tabla       : texto con la tabla (FROM)	
    @param condiciones : lista con las columnas que presentarán condiciones acompañado del comparador ('usuario = ')
    @param parametros  : lista o tuplas con parámetros que serán los valores de las condiciones.
    @returns           : cursor.
    """
    def eliminar (self, tabla, condiciones = None, parametros = []):
        consulta = f'DELETE FROM {tabla} '
        if condiciones is not None:
            consulta = consulta + " WHERE " + ' %s AND '.join (condiciones) + ' %s'
        cursor = self.ejecutar (consulta, parametros)
        self.conexion.commit()
        return cursor
        

    """
    cursor = bdd.seleccionar_union (['usuarios', 'canciones'], ['usuarios.chat_id', 'canciones.enlace'], ['usuarios.id = canciones.id'])

    """
    def seleccionar_union (self, tablas= ['usuarios', 'opciones'] , columnas = ['usuarios.*, opciones.modo'], uniones=['usuarios.id = opciones.usuario_id' ], condiciones = None , parametros = [], limite = 100, orden = None):

        # arma la la estructura SELECT.
        consulta = 'SELECT ' + ', '.join (columnas) + ' FROM ' + tablas [0] + ' '
        
        # recorre el resto de las tablas, las que se unirán en la consulta.
        # arma la estructura INNER JOIN.
        iterador_uniones = uniones.__iter__()
        for tabla in tablas [1:]:
        
            if iterador_uniones.__length_hint__()>= 0:
                consulta = consulta + ' INNER JOIN ' + tabla
                consulta = consulta + ' ON ' + iterador_uniones.__next__()
        
        # arma la estructura WHERE.
        if condiciones is not None:
            consulta = consulta + " WHERE " + ' %s AND '.join (condiciones) + ' %s'
        
        # límite
        if limite is not None:
            consulta = consulta + ' LIMIT ' + str (limite)

        
        cursor = self.ejecutar (consulta, parametros)

        if str (limite) == '1':			
            return self.obtener_uno (cursor)

        return self.obtener_todos (cursor)




    def ultimo_id (self, cursor):
        return cursor.lastrowid