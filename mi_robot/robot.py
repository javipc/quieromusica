#!/bin/python3

from mi_robot.comandos import Robot_Comandos
from asyncio           import sleep
from recursos.msj      import msj
from mi_robot.vista    import *
from recursos.archivo  import eliminar_archivo

# ABSTRACCIÓN - NO DEBE HABER COMANDOS DE DEEZER NI PYROGRAM 

class Robot (Robot_Comandos):

    def __init__ (self):
        super().__init__ ()
        self.musica      = None
        self.bdd         = None
        self.sala_copias = None
        self.usuarios_descargando = []
        
        

    
    # Esto debe ir en una configuración
    filtro_genero  = ['cumbia', 
                      'reggaeton', 
                      'reggaetón', 
                      'regueton', 
                      'reguetón', 
                      'bachata' ]

    filtro_artista = [  'abraham mateo',
                        'adassa',
                        'aitana',
                        'alberto stylee',
                        'aldo ranks',
                        'alemán',
                        'alexis', 'fido',
                        'ana mena',
                        'andy rivera',
                        'khriz',
                        'anitta',
                        'anuel aa',
                        'arcángel', 'de la ghetto',
                        'baby ranks',
                        'baby rasta', 'gringo',
                        'bad bunny',
                        'bad gyal',
                        'becky g',
                        'bizarrap',
                        'brytiago',
                        'blessd',
                        'justin quiles',
                        'lenny tavarez',
                        'c-kan',
                        'cali', 'el dandee',
                        'calle 13',
                        'calle ciega',
                        'cauty',
                        'cazzu',
                        'chimbala',
                        'chino', 'nacho',
                        'chosen few',
                        'cnco',
                        'corina smith',
                        'cosculluela',
                        'crooked stilo',
                        'cuban link',
                        'cubanito',
                        'daddy yankee',
                        'dalex',
                        'danna paola',
                        'danny ocean',
                        'darell',
                        'de la ghetto',
                        'deevani',
                        'denise rosenthal',
                        'divino',
                        'dj nelson',
                        'dj playero',
                        'doble a', 'nales',
                        'doble a',
                        'don chezina',
                        'don miguelo',
                        'don omar',
                        'duki',
                        'echo',
                        'eddy lover',
                        'eddy-k',
                        'el alfa',
                        'el chombo',
                        'el dandee',
                        'el general',
                        'el medico',
                        'el roockie',
                        'emilia',
                        'enrique iglesias',
                        'erre xi',
                        'faraón love shady',
                        'farina',
                        'farruko',
                        'feid',
                        'fito blanko',
                        'flex',
                        'francinne',
                        'gaby',
                        'gente de zona',
                        'getto', 'gastam',
                        'glory',
                        'greeicy',
                        'guelo star',
                        'heavy clan',
                        'héctor el father',
                        'ingratax',
                        'ivy queen',
                        'j alvarez',
                        'j balvin',
                        'j mena',
                        'j quiles',
                        'jadiel',
                        'jamsha',
                        'jhay cortez',
                        'joey montana',
                        'johnny prez',
                        'jon z',
                        'jowell', 'randy',
                        'juanes',
                        'k-narias',
                        'kali uchis',
                        'karol g',
                        'kendo kaponi',
                        'kenia os',
                        'kevin roldán',
                        'khea',
                        'kim loaiza',
                        'la factoría',
                        'la sista',
                        'la tigresa del oriente',
                        'lali',
                        'lalo ebratt',
                        'las guanábanas',
                        'latin fresh',
                        'lele pons',
                        'lele',
                        'leslie grace',
                        'leslie shaw',
                        'lisa m',
                        'lit killah',
                        'lito', 'polaco',
                        'lola indigo',
                        'lorna',
                        'los tres mosqueteros',
                        'luis fonsi',
                        'luísa sonza',
                        'lumidee',
                        'lunay',
                        'luny tunes',
                        'lyanno',
                        'magnate', 'valentino',
                        'maicol', 'manuel',
                        'makano',
                        'mala rodriguez',
                        'maluma',
                        'manuel turizo',
                        'maría becerra',
                        'mariah angeliq',
                        'mario vi',
                        'master joe', 'o.g. black',
                        'mc ceja',
                        'mc davo',
                        'mey vidal',
                        'miguelito',
                        'monserrate', 'dj urba',
                        'mozart la para',
                        'ms nina',
                        'n.o.r.e.',
                        'naldo',
                        'nales',
                        'nando boom',
                        'nathy peluso',
                        'natti natasha',
                        'nely',
                        'nesi',
                        'nicki nicole',
                        'nicky jam',
                        'nina sky',
                        'nio garcia',
                        'notch',
                        'nova', 'jory',
                        'noztra',
                        'ñejo', 'dalmata',
                        'ñengo flow',
                        'o.g. black',
                        'omar montes',
                        'oriana',
                        'osmani garcía',
                        'ovi',
                        'ozuna',
                        'pabllo vittar',
                        'pablo chill-e',
                        'paloma mami',
                        'paulo londra',
                        'pescozada',
                        'piso 21',
                        'pitbull',
                        'plan b',
                        'prince royce',
                        'ptazeta',
                        'r.k.m', 'ken-y',
                        'ranking stone',
                        'rauw alejandro',
                        'renée and renato',
                        'reykon',
                        'romeo santos',
                        'rosalía',
                        'sebastián yatra',
                        'sech',
                        'snoop lion',
                        'snow tha product',
                        'sofia reyes',
                        'speedy',
                        'tainy',
                        'tego calderón',
                        'temperamento',
                        'tempo',
                        'thalía',
                        'tiago pzk',
                        'tini',
                        'tito el bambino',
                        'tony dize',
                        'tony touch',
                        'tony touch',
                        'torombolo',
                        'trebol clan',
                        'trueno',
                        'turreo',
                        'vico c',
                        'voltio',
                        'wendy sulca',
                        'wibal', 'alex',
                        'wisin', 'yandel',
                        'wos',
                        'yaga', 'mackie',
                        'yomo',
                        'zato dj',
                        'zion', 'lennox']


    def filtro_audio (self, audio):
        if 'prohibido' in audio.keys():
            return 'Prohibido'
        for genero in self.filtro_genero:
            if genero in audio ['genero'].lower():
                return genero
            if genero in audio ['cancion'].lower():
                return genero

        return self.filtro (audio ['artista'], self.filtro_artista + self.filtro_genero) 
        

    def filtro_texto (self, texto):        
        for genero in self.filtro_genero:
            if genero in texto.lower():
                return genero
        
        return self.filtro (texto, self.filtro_artista + self.filtro_genero) 
        
            
            


    def filtro (self, texto, lista):
        texto = texto.lower () .replace('&', 'y')
        if texto in lista:
            return texto
        for parte_texto in texto.split (' y '):
            if parte_texto in lista:
                return parte_texto            
        return None

    
    # esto hay que ubicar en otro lugar
    async def detener_usuario (self, usuario):                
        print ('--- CONSULANDO USUARIOS', self.usuarios_descargando)
        while usuario in self.usuarios_descargando:
            print (" USUARIO YA EXISTE, AHORA ESTÁ BLOQUEADO - usuario:", usuario , 'lista: ', self.usuarios_descargando)
            await sleep (5)
        self.usuarios_descargando.append (usuario)
        print ('--- USUARIO agregado', self.usuarios_descargando)

    def usuario_esta_bloqueado (self, usuario):
        return usuario in self.usuarios_descargando

    def bloquear_usuario (self, usuario):
        if usuario not in self.usuarios_descargando:
            self.usuarios_descargando.append (usuario)
        print ('--- USUARIO agregado', self.usuarios_descargando)

    def desbloquear_usuario (self, usuario):        
        self.usuarios_descargando.remove (usuario)
        print ('--- USUARIO eliminado', self.usuarios_descargando)


    







    """
        @param chat_id : texto, el usuario que descarga, o a quién se destina la descarga.
        @param enlace  : texto, enlace de descarga.
        @param calidad : texto, el formato y calidad de descarga.
    """
    async def descargar (self, chat_id, enlaces, calidad):
        msj ('DESCARGAS: chat_id ', chat_id, 'calidad', calidad, 'enlaces', enlaces)
        ruta = 'descargas/' + str(chat_id)
    

        # base de datos
        # anota una descarga para el usuario.
        for enlace in enlaces:
            if self.bdd is not None:                        
                self.bdd.insertar_descarga_pendiente (chat_id, enlace, calidad)

        

        for enlace in enlaces:
            # Noficación.
            await self.enviar_accion (chat_id, 'grabacion')



            audio  = None
            en_bdd = False

            # Búsqueda en la base de datos.
            # Verifica si ya existe esa descarga.
            if self.bdd is not None:
                audio = self.bdd.obtener_cancion (enlace, calidad) 
                if audio is not None:
                    audio ['archivo']  = audio ['file_id']
                    audio ['telegram'] = audio ['telegram_id']
                    audio ['nuevo_nombre'] = None
                    en_bdd = True

                    msj ('ESTÁ EN LA BASE DE DATOS.', enlace, calidad)

            # Si no está en la base de datos inicia la descarga.
            if audio is None:          
                audio = self.musica.descargar (enlace, calidad, ruta)
                    
            
            msj ("DESCARGAS: audio")
            msj (audio)

            # Si no pudo descargarse, notifica el error.
            if not audio:
                texto = crear_vista_texto (icono='fracaso')
                await self.enviar_mensaje (chat_id, texto)
                msj ("DESCARGAS: ERROR EN LA DESCARGA", enlace, calidad)
                continue

            
            # Notifica que va a subir un audio.
            mensaje_espera = await self.enviar_mensaje (chat_id, crear_vista_texto (icono='espere'))

            calidad_original = calidad
            nombre_archivo   = audio ['archivo'] # contiene el nombre de archivo a subir, o un file_id
            nuevo_nombre     = audio ['nuevo_nombre'] # si es un archivo, contiene el nuevo nombre.
            artista          = audio ['artista'] 
            cancion          = audio ['cancion'] 
            calidad          = audio ['calidad'] 
            genero           = audio ['genero'] 

            prohibido = self.filtro_audio (audio)
                
            
            # Envía el audio
            file_id = None
            respuesta_envio = None
            if prohibido:
                vista = crear_vista_prohibido ()                  
                # respuesta_envio = await self.enviar_mensaje (chat_id, texto=texto)
                respuesta_envio = await self.reenviar_mensaje (chat_id, vista['sala_origen'], vista['mensaje_origen'])
            else:
                vista = crear_vista_audio ({'calidad': calidad, 'robot': self.mi_usuario(), 'enlace': enlace })
                texto = vista ['texto']
                
                respuesta_envio = await self.enviar_audio (chat_id, nombre_archivo, texto=texto, nombre_archivo=nuevo_nombre)
                if respuesta_envio:
                    file_id = respuesta_envio.audio.file_id


            await self.borrar_mensaje (mensaje_espera)


            # limpieza de archivos
            if not en_bdd:
                eliminar_archivo (nombre_archivo)                



            if not respuesta_envio:
                await self.enviar_mensaje (chat_id, crear_vista_texto (icono='fracaso'))
                msj ("DESCARGAS: ERROR. FRACASÓ EL ENVÍO.")
                continue



        

            

            id_telegram = None

            # SI ESTÁ PROHIBIDO NO SE ENVÍA COPIAS
            if not prohibido and not en_bdd:
                
                # copias
                if self.sala_copias is not None:

                    vista = crear_vista_audio_copia ({'calidad': calidad, 'robot': self.mi_usuario(), 'enlace': enlace })
                    texto = vista ['texto']
                    try:
                        respuesta_envio = await self.enviar_audio (self.sala_copias, file_id, texto=texto, nombre_archivo=nuevo_nombre, accion=False)                    
                        id_telegram = self.obtener_id (respuesta_envio)                    
                    except:
                        msj ("COPIAS: ERROR AL INTENTAR ENVIAR COPIA A LA SALA")






            # base de datos


            if self.bdd is not None:        
                msj ("DESCARGAS: BDD.")
                # registra la descarga
                self.bdd.actualizar_descarga_completa (chat_id, enlace, calidad_original)

                if en_bdd:
                    msj ("YA ESTÁ EN BASE DE DATOS, LISTO EL PROCESO.")
                    continue
                
                try:                    
                    msj ('DESCARGAS: ID ARCHIVO', file_id)
                    self.bdd.insertar_cancion (enlace, calidad, file_id, id_telegram ,artista, cancion, genero, prohibido)
                except:
                    msj ("DESCARGAS: BDD: ERROR AL INTENTAR INSERTAR")



        msj ("DESCARGAS: Listo el proceso.")






    def obtener_canciones_botonera (self, mensaje, tipo='track'):        
        identificadores = []
        lista = mensaje.message.reply_markup.inline_keyboard        
        for botones in lista:
            for boton in botones:
                texto = boton.callback_data                
                lista = texto.partition (' ' + tipo + ' ')
                if lista [1] == '':
                    continue
                if lista [0] != '/detalle':
                    continue
                identificadores.append (lista [2])
        return identificadores






    def obtener_respuestas_botonera (self, mensaje):
        respuesta = {}
        respuesta ['texto']     = mensaje.message.text
        respuesta ['botones']   = mensaje.message.reply_markup.inline_keyboard
        respuesta ['texto']     = mensaje.message.text
        respuesta ['respuesta'] = mensaje.data
        return respuesta
        