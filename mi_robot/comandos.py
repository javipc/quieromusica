#!/bin/python3

"""

   RoBOT Música.
   
   Eventos recibidos.

   Javier Martinez,  Agosto 2022

"""





from telegram.eventos   import *
from telegram.mensajes  import *
from telegram.telegram  import Telegram
from mi_robot.vista     import *
from recursos.msj       import *
from recursos.reiniciar import reiniciar


""" 
comandos
/start
/buscar
etc ....
ABSTRACCIÓN - NO DEBE HABER COMANDOS DE DEEZER NI PYROGRAM 

"""


class Robot_Comandos (Telegram):


    # asocia las funciones con eventos
    def __init__ (self):
        super ().__init__ ()


        self.evento_comando (self.ev_comando_buscar, ['buscar'])
        self.evento_boton_comando (self.ev_boton_buscar, '/buscar')
        
        self.evento_boton_comando (self.ev_boton_descargar_todo, '/descargar_todo')        

        self.evento_comando (self.ev_comando_descargar, ['descargar'])
        self.evento_boton_comando (self.ev_boton_descargar, '/descargar')

        self.evento_comando (self.ev_comando_detalle, ['detalle', 'info', 'informacion'])
        self.evento_boton_comando (self.ev_boton_detalle, '/detalle')
        self.evento_boton_comando (self.ev_boton_pagina, '/pagina')
      
        self.evento_comando (self.ev_comando_start, 'start')

        self.evento_comando (self.ev_comando_reiniciar, 'reiniciar')
            
        self.evento_comando (self.ev_comando_ayuda, ['ayuda', 'help', '\?'])

        # esto siempre debe ir a lo último
        self.evento_texto (self.ev_texto_buscar)

    """
    Cuando el usuario ejecuta /start
    @param cliente: recibe el cliente telegram
    @param mensaje: el mensaje recibido
    """
    async def ev_comando_start (self,cliente, mensaje):
        vista = crear_vista_start ()
        #await self.responder_video (mensaje, 'BAACAgEAAx0ERMNYqgADCGMKf7Q0m7Fk_2OiYzYHFEzuxQaeAAKeAwACG-RQREiie4H0d5ooHgQ', vista ['texto'])        
        await self.responder_reenviar (mensaje, vista['sala_origen'], vista['mensaje_origen'])
        await self.responder (mensaje, vista['texto'])

        



    """
    Cuando el usuario ejecuta /?
    @param cliente: recibe el cliente telegram
    @param mensaje: el mensaje recibido
    """
    async def ev_comando_ayuda (self,cliente, mensaje):
        vista = crear_vista_ayuda ()
        # await self.responder_video (mensaje, 'BAACAgEAAx0ERMNYqgADCGMKf7Q0m7Fk_2OiYzYHFEzuxQaeAAKeAwACG-RQREiie4H0d5ooHgQ', vista ['texto'])
        await self.responder_reenviar (mensaje, vista['sala_origen'], vista['mensaje_origen'])
        await self.responder (mensaje, vista['texto'])

        
                


    """
    Cuando el usuario escribe un mensaje de texto
    @param cliente: recibe el cliente telegram
    @param mensaje: el mensaje recibido
    """
    async def ev_texto_buscar (self,cliente, mensaje):
        if self.es_comando (mensaje):
            return

        await self.responder_accion (mensaje)

        texto = self.obtener_texto (mensaje)

        # primero debe verificar que no sea un enlace
        filtro_encontrado = self.filtro_texto (texto)
        if filtro_encontrado is not None:
            vista = crear_vista_prohibido (texto)
            respuesta_envio = await self.responder_copiar (mensaje, vista['sala_origen'], vista['mensaje_origen'], vista ['texto'])
            return

        # si es un texto, realiza una búsqueda        
        lista_respuesta = self.musica.buscar (texto)
            
        msj ("lista")
        msj (lista_respuesta)
        msj ("respuestas")
        
        if not lista_respuesta:            
            texto = crear_vista_texto (icono='fracaso')
            await self.responder (mensaje, texto)
            return
        vista = crear_vista_resultado (lista_respuesta)        
        await self.responder (mensaje, vista ['texto'], botones=vista['botones'])

                



    async def ev_buscar (self, cliente, mensaje):
        await self.responder_accion (mensaje)
        parametros = self.obtener_parametro (mensaje)
        parametros = parametros.split(maxsplit=1)
        tipo       = 'track'
        busqueda   = parametros [0]
        
        if len (parametros) >= 2:            
            for tipo_busqueda in ['track', 'album', 'artist', 'playlist', 'cancion', 'artista', 'album', 'lista']:
                if parametros [0] == tipo_busqueda:
                    tipo     = parametros [0]
                    busqueda = parametros [1]
                    break
        
        if tipo == 'cancion':
            tipo = 'track'
        if tipo == 'artista':
            tipo = 'artist'
        if tipo == 'lista':
            tipo = 'playlist'

        
        filtro_encontrado = self.filtro_texto (busqueda)
        if filtro_encontrado is not None:
            vista = crear_vista_prohibido (busqueda)
            respuesta_envio = await self.responder_copiar (mensaje, vista['sala_origen'], vista['mensaje_origen'], vista ['texto'])
            return

        # si es un texto, realiza una búsqueda        
        lista_respuesta = self.musica.buscar (busqueda, tipo)
            
        msj ("lista")
        msj (lista_respuesta)
        msj ("respuestas")
        
        if not lista_respuesta:            
            texto = crear_vista_texto (icono='fracaso')
            await self.responder (mensaje, texto)
            return
        vista = crear_vista_resultado (lista_respuesta)        
        await self.responder (mensaje, vista ['texto'], botones=vista['botones'])




    async def ev_boton_buscar (self, cliente, mensaje):
        await self.ev_buscar (cliente, mensaje)
    async def ev_comando_buscar (self, cliente, mensaje):
        await self.ev_buscar (cliente, mensaje)





    """
    Cuando el usuario ejecuta /detalle <TIPO> <ID>
    Se obtiene una imagen y debaje un detalle y opciones.
    /detalle cancion 2341
    /detalle album 0101    
    @param cliente: recibe el cliente telegram
    @param mensaje: el mensaje recibido
    """

    async def ev_comando_detalle (self, cliente, mensaje):
        await self.ev_detalle (cliente, mensaje)
    
    async def ev_boton_detalle (self, cliente, mensaje):
        await self.ev_detalle (cliente, mensaje)

    async def ev_detalle (self, cliente, mensaje):
        parametros = self.obtener_parametro (mensaje)
        parametro  = parametros.split()
        if len (parametro) < 2:
            return
        tipo = parametro [0]
        identificador = parametro [1]
        if identificador.isalnum () == False:
            return

        await self.responder_accion (mensaje)
        detalle = self.musica.obtener_detalle (identificador, tipo)
        
        msj ('botón', mensaje)
        msj ("parametro")
        msj (parametro)

        msj ("detalle")
        msj (detalle)
        
        vista = crear_vista_detalle (tipo, detalle)
        # botonera = crear_botonera_detalle (lista_respuesta)
        texto   = None
        botones = None
        imagen  = None

        msj ("vista")
        msj (vista)


        if 'texto'   in vista.keys():
            texto   = vista ['texto']
        if 'botones' in vista.keys():
            botones = vista ['botones']
        if 'imagen'  in vista.keys():
            imagen  = vista ['imagen']

        
        filtro_encontrado = self.filtro_texto (detalle ['artista'])
        if filtro_encontrado is not None:
            vista = crear_vista_prohibido (filtro_encontrado)
            respuesta_envio = await self.responder_copiar (mensaje, vista['sala_origen'], vista['mensaje_origen'], vista ['texto'])
            return
        
        msj ('texto ',texto )
        if imagen is not None:
            await self.responder_imagen (mensaje, texto=texto, botones=botones, imagen = imagen)
        else:
            await self.responder (mensaje, texto=texto, botones=botones)


    """
    Cuando el usuario ejecuta /descargar <calidad> <enlace> <enlace> <enlace> ... <enlace> 
    /descargar calidad id
    /descargar calidad enlace
    /descargar enlace
    @param cliente: recibe el cliente telegram
    @param mensaje: el mensaje recibido
    """
    async def ev_descargar (self, cliente, mensaje):

        
        parametros = self.obtener_parametro (mensaje)
        parametro = parametros.split()
        msj ('DESCARGAS: parametro', parametro)

        if len (parametro) < 1:
            return

        
        chat_id = self.obtener_remitente_id (mensaje)

        # Para un único parámetro, asume que es un enlace.
        enlaces  = [ parametro [0] ]
        calidad = 'FLAC'
        
        # Para más de un parámetro, asume hay calidad y uno o más enlaces.
        # TODO podría no haber calidad y asumir que es FLAC

        if len (parametro) > 1:
            calidad = parametro [0]
            enlaces = parametro [1:]

        
        if self.usuario_esta_bloqueado (chat_id):
            texto = crear_vista_texto (icono='denegado')
            await self.responder_boton (mensaje, texto)
            return
        self.bloquear_usuario (chat_id)

        # for enlace in enlaces:
        #     info_enlace = self.musica.obtener_info_enlace (enlace)
        #     if info_enlace is not None:
        #         for tipo in ['track', 'album', 'playlist']:
        #             if info_enlace ['tipo'] in enlace:
        #                 conjunto_enlaces = self.musica.obtener_detalle (enlace)
        #                 
        #                 break
        
        await self.descargar (chat_id, enlaces, calidad)

        self.desbloquear_usuario (chat_id)
        


    async def ev_boton_descargar (self, cliente, mensaje):
        texto = crear_vista_texto (icono='descargar')
        await self.responder_boton (mensaje, texto)
        await self.ev_descargar    (cliente, mensaje)
        

    async def ev_comando_descargar (self, cliente, mensaje):
        await self.ev_descargar (cliente, mensaje)
        self.desbloquear_usuario (remitente_id)




    async def ev_descargar_todo (self, cliente, mensaje):
        msj ('descargando todo')
        identificadores = self.obtener_canciones_botonera (mensaje)
        enlaces = self.musica.a_enlaces (identificadores, 'track')

        
        calidad = self.obtener_parametro (mensaje).strip()
        if calidad == '':
            calidad = 'FLAC'
        
        chat_id = self.obtener_remitente_id (mensaje)
        msj ('ID', identificadores)
        msj ('enlaces', enlaces)
        if self.usuario_esta_bloqueado (chat_id):
            texto = crear_vista_texto (icono='denegado')
            await self.responder_boton (mensaje, texto)
            return
        self.bloquear_usuario (chat_id)

        msj ('DESCAGANDO')
        await self.descargar (chat_id, enlaces, calidad)

        self.desbloquear_usuario (chat_id)
        


    async def ev_boton_descargar_todo (self, cliente, mensaje):
        texto = crear_vista_texto (icono='descargar')
        await self.responder_boton (mensaje, texto)
        await self.ev_descargar_todo    (cliente, mensaje)
        


    async def ev_pagina (self, cliente, mensaje):
        parametros = self.obtener_parametro (mensaje)
        parametros = parametros.split (maxsplit=1)
        respuesta_botones = self.obtener_respuestas_botonera (mensaje)
        tipo   = parametros [0]
        pagina = parametros [1]       
        texto = respuesta_botones ['texto']
        busqueda = texto.split (': ', maxsplit=1)[1]

        print (parametros)
        print (busqueda)
        print ('????')

        await self.responder_accion (mensaje)
        
        
        lista_respuesta = self.musica.buscar (busqueda, tipo, pagina)
            
        msj ("lista")
        msj (lista_respuesta)
        msj ("respuestas")
        
        if not lista_respuesta:            
            texto = crear_vista_texto (icono='fracaso')
            await self.responder (mensaje, texto)
            return
        vista = crear_vista_resultado (lista_respuesta)        
        await self.responder (mensaje, vista ['texto'], botones=vista['botones'])




    async def ev_boton_pagina (self, cliente, mensaje):
        await self.ev_pagina (cliente, mensaje)


    async def ev_comando_reiniciar (self, cliente, mensaje):        
        remitente_id = self.obtener_remitente_id (mensaje)
        if self.propietario != self.obtener_usuario (mensaje):
            await self.enviar_mensaje (self.propietario, 'está jugando con el robot ' + str (remitente_id))
            self.bloquear_usuario (remitente_id)
            return

        await self.responder (mensaje, 'Ahora mismo.')
        reiniciar ()
        return
    
    

    """
    Cuando el usuario ejecuta /configuracion
    /configuracion calidad flac
    /configuracion calidad mp3_128
    @param cliente: recibe el cliente telegram
    @param mensaje: el mensaje recibido
    """

    # 
    # a.search_album
    # a.search_artist
    # a.search_track
    # a.search_playlist
    # a.get_track
    # a.get_playlist
    # a.get_img_url
    # a.get_chart
    # a.get_artist_top_tracks
    # a.get_artist_top_playlists
    # a.get_artist_top_albums
    # a.get_artist_related
    # a.get_artist_radio
    # a.get_artist
    # a.get_album
    # a.choose_img
    # a.tracking
    # a.tracking_album
# 
# 

