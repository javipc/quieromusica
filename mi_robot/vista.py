#!/bin/python3

"""

   RoBOT Música.
   
   Genera contenido que visualza el usuario.

   Javier Martinez,  Agosto 2022

"""




from telegram.botones import Botonera
from recursos.msj import *

emoji = {
    
'audios'    : '🎶',
'audio'     : '🎵',
'track'     : '🎵',
'album'     : '💿',
'artists'   : '👥',
'artist'    : '👤',
'artista'   : '👤',
'playlist'  : '📦',
'espere'    : '⏳',
'guardar'   : '💾',
'vista'     : '👀',
'descargar' : '📥',
'estrella'  : '⭐️',
'gratis'    : '🎁',
'donar'     : '❤️',
'flac'      : '🔊',
'mp3_320'   : '🔉',
'mp3_128'   : '🔈',
'fracaso'   : '🙁',
'no1'       : '🤷',
'no2'       : '🤷‍♀️',
'aceptar'   : '✅',
'cancelar'  : '❌',
'eliminar'  : '🗑',
'auricular' : '🎧',
'fecha'     : '📅',
'exito'     : '👌🏿',
'busqueda'  : '🔎',
'genero'    : '🎼',
'robot'     : '🤖',
'enlace'    : '🌎',
'prohibido' : '🚫',
'pregunta'  : '❓',
'denegado'  : '✋🏿',
'mas'       : '➕'
}

gif = {
'no'            : ('@Imagelandia', 254),
'no_encontrado' : ('@Imagelandia', 742)
}

video = {
'ayuda'    : ('@QuieroMusica', 8)
}

nivel = '▁ ▂ ▄ ▅ ▆ ▇ █'

formatos = ('flac', 'mp3_320', 'mp3_128')


def crear_vista_start ():
    respuesta = {}
    respuesta['texto'] = '```' + emoji['flac']    + ' = FLAC (>600 kbps) 👍🏿 \n' + emoji['mp3_320'] + ' = mp3  320 kbps \n' + emoji['mp3_128'] + ' = mp3  128 kbps ```' + ' \n ' + emoji['enlace'] + ' https://gitlab.com/javipc/mas  \n ✉️ @javicel 👋🏿'
    respuesta ['sala_origen'] = video ['ayuda'] [0]
    respuesta ['mensaje_origen'] = video ['ayuda'] [1]    
    return respuesta

def crear_vista_ayuda ():
    respuesta = {}
    respuesta['texto'] = """
💻 Creado por: Javier 👋🏿
❤️ Donaciones: https://gitlab.com/javipc/mas    
✉️ Contacto: @javicel

Créditos: 
  https://github.com/OpenJarbas/deezeridu
  https://pyrogram.org/
"""
    respuesta ['sala_origen'] = video ['ayuda'] [0]
    respuesta ['mensaje_origen'] = video ['ayuda'] [1]    
    return respuesta


"""
@param detalles: lista con respuestas de la api
@returns: lista
"""

def crear_vista_cancion (detalles):    
    botonera = Botonera()      
    msj (detalles)      
    # botonera.insertar_vertical ('💾', '/descargar ' + detalles ['enlace'])

    ## ESTA CHANCHADA NO DEBE HACERSE ASÍ, ESTA ES LA PARTE DE VISTA, NO LA DE FORMATOS, 
    ## LOS FORMATOS DISPONIBLES DEBEN VENIR EN EL PARÁMETRO detalles.
    for formato in formatos:
        botonera.insertar_horizontal (emoji['guardar'] + emoji[formato.lower()] , '/descargar ' + ' ' + formato.upper() + ' ' + detalles ['enlace'])
    
    botonera.insertar_vertical (emoji['album'] + ' ' + detalles['album_titulo'] , '/detalle album ' + str (detalles ['album_id'] ))

    for artista in detalles['artistas']: 
        print ('bucle')  
        print (artista)
        botonera.insertar_vertical (emoji['artista'] + ' ' + artista['artista'] , '/detalle artist ' + str (artista['id'] ))

    respuesta = {}
    respuesta ['texto']   = emoji ['artist'] + ' ' + detalles ['artista'] + '\n' + emoji ['track']  + ' ' + detalles ['titulo'] + '\n' + emoji ['album']  + ' ' + detalles ['album_titulo'] + '\n' + emoji ['fecha']  + ' ' + detalles ['fecha'] 
        # + '\n' + emoji ['auricular'] + ' ' + detalles ['bot_usuario']

    respuesta ['imagen']  = detalles ['album_imagen']
    respuesta ['botones'] = botonera.obtener_botones()
    return respuesta





def crear_vista_artista (detalles):    
    botonera = Botonera()            
    # botonera.insertar_vertical ('💾', '/descargar ' + detalles ['enlace'])

    ## ESTA CHANCHADA NO DEBE HACERSE ASÍ, ESTA ES LA PARTE DE VISTA, NO LA DE FORMATOS, 
    ## LOS FORMATOS DISPONIBLES DEBEN VENIR EN EL PARÁMETRO detalles.
    
    
    botonera.insertar_horizontal (emoji['album'] , '/detalle albumes ' + str (detalles ['id'] ))
    botonera.insertar_horizontal (emoji['audio'] , '/detalle canciones ' + str (detalles ['id'] ))
    
    respuesta = {}
    respuesta ['texto']   = emoji ['artist'] + ' ' + detalles ['artista'] 
        # + '\n' + emoji ['auricular'] + ' ' + detalles ['bot_usuario']

    respuesta ['imagen']  = detalles ['imagen']
    respuesta ['botones'] = botonera.obtener_botones()
    return respuesta    






def crear_vista_album (detalles):
    botonera = Botonera()            
    respuesta = {}
    respuesta ['texto'] = emoji ['album']  + ' ' + detalles ['titulo'] + '\n' + emoji ['fecha']  + ' ' + detalles ['fecha'] + '\n' + emoji ['audios']  + ' ' + str (detalles ['total']) 
            
        
    for genero in detalles ['generos']:
        respuesta ['texto'] = respuesta ['texto'] + '\n' + emoji ['genero']  + ' ' + genero ['genero']



    
    for cancion in detalles ['canciones'][0:95]:
        msj ('cancion')
        msj (cancion)
        botonera.insertar_vertical (emoji['track'] + ' ' + cancion ['artista'] + ' - ' + cancion['titulo'] , '/detalle track ' + str (cancion ['id'] )) 

            

    for formato in formatos:
        botonera.insertar_horizontal (emoji['guardar'] + emoji[formato.lower()] , '/descargar_todo ' + formato.upper() ) # + detalles ['enlace'])

    
        # + '\n' + emoji ['auricular'] + ' ' + detalles ['bot_usuario']

    respuesta ['imagen']  = detalles ['imagen']
    respuesta ['botones'] = botonera.obtener_botones ()
    return respuesta





def crear_vista_lista (detalles):
    botonera = Botonera()            
    respuesta = {}
    respuesta ['texto'] = emoji ['playlist']  + ' ' + detalles ['titulo'] + '\n' + emoji ['fecha']  + ' ' + detalles ['fecha'] + '\n' + emoji ['audios']  + ' ' + str (detalles ['total']) 
                        

    
    for cancion in detalles ['canciones'][0:95]:
        msj ('cancion')
        msj (cancion)
        botonera.insertar_vertical (emoji['track'] + ' ' + cancion ['artista'] + ' - ' + cancion['titulo'] , '/detalle track ' + str (cancion ['id'] )) 

            

    for formato in formatos:
        botonera.insertar_horizontal (emoji['guardar'] + emoji[formato.lower()] , '/descargar_todo ' + formato.upper() ) # + detalles ['enlace'])

    
        # + '\n' + emoji ['auricular'] + ' ' + detalles ['bot_usuario']

    respuesta ['imagen']  = detalles ['imagen']
    respuesta ['botones'] = botonera.obtener_botones ()
    return respuesta



def crear_vista_detalle (tipo='track', detalles=[]):
    if tipo == 'track':
        return crear_vista_cancion (detalles)

    if tipo == 'album':
        return crear_vista_album (detalles)

    if tipo == 'artist':
        return crear_vista_artista (detalles)

    if tipo == 'playlist':
        return crear_vista_lista (detalles)

    
    if tipo == 'albumes' or tipo == 'canciones':
        return crear_vista_resultado (detalles)

    return None



def crear_vista_audio (detalles):
    respuesta = {}    
    respuesta['texto'] = ' ' + emoji ['auricular'] + ' ' + detalles['calidad'] + ' \n ' + emoji['robot'] + ' @' + detalles['robot'] # + '\n '  + detalles['enlace']
    return respuesta
        

def crear_vista_audio_copia (detalles):
    respuesta = {}
    respuesta['texto'] = ' ' + emoji ['auricular'] + ' ' + detalles['calidad'] +  '\n ' + detalles['enlace']    
    return respuesta


def crear_vista_texto (icono, texto=None):
    if texto is not None:
        return emoji [icono] + ' ' + texto
    return emoji [icono] 


def crear_vista_prohibido (texto=''):
    respuesta = {}
    respuesta ['sala_origen']    = gif ['no'] [0]
    respuesta ['mensaje_origen'] = gif ['no'] [1]        
    respuesta ['texto']          = ' ' + emoji ['prohibido'] + ' ' + texto
    return respuesta

    



"""
crea una botonera con resultados de búsquedas
@param lista: listado en español con datos para crear una botonera de música
"""
def crear_vista_resultado (lista):
    print ('-----')
    print ('-----')
    print ('-----')
    print (lista)
    print ('-----')
    botonera = Botonera()
    for respuesta in lista['data']:
        texto = ''
        if 'tipo' in respuesta.keys():
            texto = texto + emoji [respuesta['tipo']]
        texto = texto + " "
        texto = texto + respuesta['texto']        
        
        botonera.insertar_vertical (texto, '/detalle ' + respuesta['tipo'] + ' ' + str (respuesta['id']))
    
    if 'siguiente' in lista.keys() :
        botonera.insertar_vertical (emoji ['mas'], '/pagina ' + lista ['tipo'] + ' ' + lista ['siguiente'])    

    respuesta = {}
    respuesta ['texto'] = emoji ['busqueda'] + ' : ' + str (lista ['total'])
    if lista ['consulta'] is not None:
        respuesta ['texto'] = emoji ['busqueda'] + ' : ' + lista ['consulta']
    respuesta ['botones'] = botonera.obtener_botones()
    return respuesta




"""
@param enlace: un enlace de deezer o servicio de música
@return list: id, tipo de enlace (álbum, tema, lista de reproducción), prestador, 
"""

def datos_de_enlace (enlace, tipo = None):
    enlace=enlace.strip().lower().strip ('/')    
    arreglo = enlace.split ('/')
    if len (arreglo < 3):
        return None
    if arreglo[-1].isnumeric() == False:
        return None
    if tipo is not None:
        if tipo != arreglo[-2]:
            return None
            
    respuesta = []
    respuesta ['id']       = arreglo[-1]
    respuesta ['tipo']     = arreglo[-2]
    respuesta ['servicio'] = arreglo[-3]
    return respuesta
    
def id_enlace (enlace, tipo = None):
    enlace=enlace.strip().lower().strip ('/')    
    arreglo = enlace.split ('/')
    if len (arreglo < 3):
        return None
    if arreglo[-1].isnumeric() == False:
        return None
    if tipo is not None:
        if tipo != arreglo[-2]:
            return None
            
    return arreglo[-1]

