#!/bin/python3

"""

   RoBOT Música.
   
   Maneja la base de datos.

   Javier Martinez,  Agosto 2022

"""






from recursos.opciones import opciones
configuracion_robot = opciones ('configuracion/robot.ini', 'robot')
tipo_bdd         = configuracion_robot.obtener ('bdd', 'sqlite3')

if tipo_bdd == 'mysql':	
	from bdd.bdd_mysql import Bdd
if tipo_bdd == 'sqlite3':	
	from bdd.bdd_sqlite import Bdd	



class Bdd_Robot (Bdd):

	def __init__ (self, nombre_archivo_configuracion = 'configuracion/bdd.ini'):
		super().__init__ ()
		configuracion_bdd = opciones (nombre_archivo_configuracion, 'conexion')

		base      = configuracion_bdd.obtener ('base'     , None)
		usuario   = configuracion_bdd.obtener ('usuario'  , None)
		clave     = configuracion_bdd.obtener ('clave'    , None)
		direccion = configuracion_bdd.obtener ('direccion', None)
		 
		self.conectar (base=base, usuario = usuario, clave=clave, direccion=direccion)
		self.crear_base ()
		


	"""

			¡ESTO ES UNA ABSTRACCIÓN!

			acá no debería haber código SQL


			... pero hay


	"""	

	def crear_base (self):		
		

		# Contiene las canciones (identificadores de Telegram)
		self.ejecutar ("CREATE TABLE IF NOT EXISTS canciones ( \
				id        INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL, \
				artista   VARCHAR (255) DEFAULT ''      , \
				cancion   VARCHAR (255) DEFAULT ''      , \
				enlace    VARCHAR (255) UNIQUE NOT NULL , \
				detalle   VARCHAR (255) DEFAULT ''      , \
				genero    VARCHAR (255) DEFAULT ''      , \
				prohibido BOOLEAN       DEFAULT false ) ")

		self.ejecutar ("CREATE TABLE IF NOT EXISTS archivos ( \
				id          INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL, \
				id_cancion  INTEGER       DEFAULT 0  , \
				formato     VARCHAR (255)            , \
				telegram_id VARCHAR (100) DEFAULT '' , \
				file_id     VARCHAR (255) )")

		# Contiene información de usuarios
		self.ejecutar ("CREATE TABLE IF NOT EXISTS usuarios (\
				id         INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL, \
				chat_id    VARCHAR (100) UNIQUE NOT NULL              , \
				nombre     VARCHAR (100) DEFAULT ''                   , \
				formato    VARCHAR (10)  DEFAULT 'flac'               , \
				comprimido BOOLEAN       DEFAULT false                , \
				idioma     VARCHAR (10)  DEFAULT 'es'                 , \
				fecha      DATE          DEFAULT CURRENT_TIMESTAMP    , \
				api        VARCHAR (20)                               , \
				categoria  INTEGER       DEFAULT 0                    , \
				suspendido BOOLEAN       DEFAULT false )")

		# descargas
		self.ejecutar ("CREATE TABLE IF NOT EXISTS descargas (\
				id        INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL , \
				chat_id   VARCHAR(100) NOT NULL                       , \
				enlace    VARCHAR(100) NOT NULL                       , \
				calidad   VARCHAR(50)  DEFAULT 'FLAC'                 , \
				pendiente BOOLEAN      DEFAULT true                   , \
				date      DATE         DEFAULT CURRENT_TIMESTAMP      , \
				UNIQUE    (chat_id, enlace, calidad)) ")

		self.conexion.commit ()		
		

		self.msj  ("base de datos inicada.")


	"""
	Obtiene identificadores (file_id) de la base de datos.
	@param enlace  : texto, URL de la canción.
	@param calidad : texto, formato_calidad del audio.
	@returns       : None | -1 | texto, identificador de archivo en Telegram (file_id)
	"""
#	
	
	def obtener_identificador (self, enlace, calidad):
		self.msj  ("consultando canciones", enlace, calidad )
		
		respuesta = self.seleccionar_union (
			tablas      = ['canciones', 'archivos'], 
			columnas    = ['file_id', 'prohibido'], 
			uniones     = ['archivos.id_cancion = canciones.id'],
			condiciones = ['archivos.formato = ', 'canciones.enlace = '],
			parametros  = [calidad.upper(), enlace],
			limite = 1
			)
		if respuesta is None:
			return None
		if respuesta [1]:
			return -1 # prohibido
		return respuesta [0]
		


	"""
	Obtiene un tema 
	@param enlace  : texto, URL de la canción.
	@param calidad : texto, formato_calidad del audio.
	@returns       : None | diccionario.
	"""
	
	def obtener_cancion (self, enlace, calidad):
		self.msj  ("consultando canciones", enlace, calidad )
		
		respuesta = self.seleccionar_union (
			tablas      = ['canciones', 'archivos'], 
			columnas    = [
							'archivos.file_id',
							'archivos.telegram_id',  
							'archivos.formato',
							'canciones.artista',
							'canciones.cancion',
							'canciones.enlace',
							'canciones.detalle',
							'canciones.genero',
							'canciones.prohibido' ], 
			uniones     = ['archivos.id_cancion = canciones.id'],
			condiciones = ['archivos.formato = ', 'canciones.enlace = '],
			parametros  = [calidad.upper(), enlace],
			limite = 1
			)
		if respuesta is None:
			return None


		audio = {}
		audio ['file_id']     = respuesta [0]
		audio ['telegram_id'] = respuesta [1]
		audio ['calidad']     = respuesta [2]
		audio ['artista']     = respuesta [3]
		audio ['cancion']     = respuesta [4]
		audio ['enlace']      = respuesta [5]
		audio ['detalle']     = respuesta [6]
		audio ['genero']      = respuesta [7]
		audio ['prohibido']   = respuesta [8]

		return audio





	"""
	Agrega temas a la base de datos
	@param enlace         : texto, URL de la canción.
	@param calidad        : texto, formato_calidad del audio.
	@param file_id        : texto, identificador de archivo en Telegram.
	@param nombre_artista : texto, información adicional sobre el dato que se inserta.
	@param nombre_cancion : texto, información adicional sobre el dato que se inserta.
	@param detalles       : texto, información adicional sobre el dato que se inserta.
	"""

	



	def insertar_cancion (self, enlace, calidad, file_id, telegram_id= '', artista = '', cancion = '', detalle ='', prohibido=False):

		id_cancion = self.seleccionar ('canciones', ['id'], ['enlace ='], [enlace], limite=1)
		
		if id_cancion is None:
			self.msj  ("Agregando", enlace, calidad , file_id)
			cursor = self.insertar (
				#tabla
				'canciones', 
				#columna
				['enlace', 'artista', 'cancion', 'detalle', 'prohibido'],
				#parámetros
				[enlace, artista, cancion, detalle, prohibido] )

			id_cancion = self.ultimo_id (cursor) 
		
		else:
			id_cancion = id_cancion[0]



		respuesta = self.insertar (
			#tabla
			'archivos', 
			#columna
			['id_cancion', 'formato', 'file_id', 'telegram_id'],
			#parámetros
			[id_cancion, calidad.upper(), file_id, telegram_id] )
		
		return respuesta

		



	def insertar_descarga_pendiente (self, chat_id, enlace, calidad):
				
		self.msj  ("Rregistrando descarga", chat_id, enlace, calidad)
		cursor = self.insertar (
			#tabla
			'descargas', 
			#columna
			['chat_id', 'enlace', 'calidad', 'pendiente'],
			#parámetros
			[chat_id, enlace, calidad, True] )	
		
		return cursor

		
	def actualizar_descarga_completa (self, chat_id, enlace, calidad):
		self.msj  ("Actualizando", chat_id, enlace)
		cursor = self.actualizar (
			#tabla
			'descargas', 
			#columna
			['pendiente'],
			#parámetros
			[False] ,
			# condiciones
			['chat_id =' , 'enlace =', 'calidad ='],
			#parámetros
			[chat_id, enlace, calidad])
		
		return cursor

		
	def obtener_descargas_pendientes (self):
		self.msj  ("Obteniendo descargas pendientes")
		cursor = self.seleccionar (
			# tabla
			'descargas',
			# columnas
			['chat_id', 'enlace', 'calidad'],
			# condiciones
			['pendiente ='],
			# parámetros
			[True]
		)

		return cursor