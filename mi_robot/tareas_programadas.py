
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from recursos.reiniciar import reiniciar


class TareaProgramada:
    def __init__ (self, robot):
        self.robot = robot
        self.bdd = robot.bdd


    def iniciar_saludo (self, segundos=2):
        self.tarea_saludar = AsyncIOScheduler ()
        self.tarea_saludar.add_job(self.saludar, "interval", seconds=segundos)
        self.tarea_saludar.start()


    def iniciar_descargas_pendientes (self, segundos=5):
        if self.bdd is None:
            return
        self.tarea_descargar = AsyncIOScheduler ()
        self.tarea_descargar.add_job(self.enviar_descargas_pendientes, "interval", seconds=segundos)
        self.tarea_descargar.start()

    def iniciar_bomba (self, segundos=120, limite=2):
        self.limite      = limite
        self.contador    = limite
        self.tarea_bomba = AsyncIOScheduler ()
        self.tarea_bomba.add_job(self.bomba, "interval", seconds=segundos)
        self.tarea_bomba.start()


    async def enviar_descargas_pendientes (self):        
        self.tarea_descargar.shutdown(wait=False)
        print ('Comprobando descargas pendientes...')
        lista_pendientes = self.bdd.obtener_descargas_pendientes ()
        if lista_pendientes is None:
            print ('No hay descargas pendientes (:')
            return
        for chat_id, enlace, calidad in lista_pendientes:
            print ('REANUNDANDO DESCARGA: ', chat_id, calidad, enlace)
            self.robot.bloquear_usuario (chat_id)
            await self.robot.descargar  (chat_id, [enlace], calidad)
            self.robot.desbloquear_usuario (chat_id)


    async def saludar (self):
        self.tarea_saludar.shutdown(wait=False)
        if self.robot.propietario is not None:
            await self.robot.enviar_mensaje (self.robot.propietario, '¡Hola!')



    async def bomba (self):
        print ('tic', self.contador)
        try:
            mensaje = await self.robot.enviar_mensaje (self.robot.propietario, '🏓') # Envía un mensaje al más lindo de la sala.
        except:
            mensaje = None
        if mensaje is not None:                                # Si el mensaje fue recibido:
            await self.robot.borrar_mensaje (mensaje)            # Elimina el mensaje (no llena el privado)
            self.contador = self.limite
            print ('toc')            
            return
        self.contador = self.contador -1
        if self.contador < 0:
            print ('BOOM')
            reiniciar ()

        
    

