nombre_capsula="robot_musica"
nombre_db=$nombre_capsula"_db"
nombre_aplicacion=$nombre_capsula"_aplicacion"

echo "Creando cápsula $nombre_capsula ..."
podman pod exists $nombre_capsula || \
podman pod create \
    --name $nombre_capsula \
    

echo "Creando contenedor de base de datos $nombre_db ..."
podman container exists $nombre_db || \
podman container create \
    -e MARIADB_ROOT_PASSWORD=hola \
    -e MYSQL_DATABASE=aplicacion \
    --pod $nombre_capsula \
    --name $nombre_db \
    docker.io/library/mariadb:10.7


echo "Creando imagen ..."
podman image exists localhost/python_musica || \
	podman build -t python_musica -f musica.imagen


echo "Creando contenedor de aplicacion $nombre_aplicacion ..."

podman container exists $nombre_aplicacion || \
podman create \
    --pod $nombre_capsula \
    --name $nombre_aplicacion \
    -v /javier/codigo/podman/_compartido/:/home/javi/_compartido \
    -v /javier/codigo/python/quieromusica/:/home/codigo/robot \
    localhost/python_musica:latest

echo Listo:
podman pod ls



podman pod start $nombre_capsula
podman start $nombre_aplicacion 
podman exec -it $nombre_aplicacion bash 
