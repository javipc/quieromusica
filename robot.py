#!/bin/python3


"""

   Robot de música para Telegram.
   Basado en pyrogram.

   Javier Martinez,  Agosto 2022

   
    * directorios *
    
    musica   - programas de deezer.
    telegram - programas de pyrogram.
    recursos - utilidades.
    bdd      - manejo de base de datos.
    mi_robot - unión: pyrogram + deezer + base de datos. [ABSTRACCIÓN] no deben aparecer programas de pyrogram ni de deezer y menos de bases de datos.


"""

from recursos.opciones  import opciones
configuracion_robot = opciones ('configuracion/robot.ini', 'robot')
propietario         = configuracion_robot.obtener ('propietario', None)
sala_copias         = configuracion_robot.obtener ('copias', 'QuieroMusica')
saludar             = configuracion_robot.obtener ('saludar', True)
                    

if propietario is None:
    print ('NO HAY PROPIETARIO')
    quit()
    exit()


from mi_robot.robot     import Robot
from mi_robot.bdd       import Bdd_Robot
from musica.deezer      import Deezer
from recursos.reiniciar import reiniciar




robot             = Robot     ()    # Crea el robot
robot.musica      = Deezer    ()    # Le pasa el objeto de música al robot para que lo use.
robot.bdd         = Bdd_Robot ()    # Le pasa el objeto de base de datos al robot para que lo use.
robot.propietario = propietario     # El usuario que puede enviar comandos de administrador.
robot.sala_copias = sala_copias     # Se asigna un grupo o canal para almacenar una copia de las descargas.


if robot.musica.cliente_descarga is None:
    print ('NO HAY CLIENTE DE DESCARGA, se reiniciará en 60 segundos.')
    reiniciar (60)

"""
Temporizador que controla periódicamente que el robot está respondiendo.
"""



# Reinicio automático:
# Si no puede enviar mensajes deja que la bomba explote y reinicie el programa.
from recursos.bomba import Bomba
def vigilador ():    
    mensaje = robot.enviar_mensaje_sincrono (propietario, '🏓') # Envía un mensaje al más lindo de la sala.
    if mensaje is not None:                                # Si el mensaje fue recibido:
        robot.borrar_mensaje_sincrono (mensaje)            # Elimina el mensaje (no llena el privado)
        return True                                        # Impide la detonación por 60 segundos.
    return False                                           # Permite que explote la bomba

bomba           = Bomba ()  # Objeto Bomba.
bomba.tic       = True      # Muestra mensajes tic / toc.
bomba.tiempo    = 60        # Intervalo de vigilancia.
bomba.vigilante = vigilador # El programa de vigilancia.
bomba.activar ()





from mi_robot.tareas_programadas import TareaProgramada
tarea = TareaProgramada (robot)
if saludar:
    tarea.iniciar_saludo (15)              # Envía "Hola" para saber que se ha iniciado.
tarea.iniciar_descargas_pendientes(30) # Revisa si hay descargas pendientes.




    

# Bucle
robot.ejecutar()

#asyncio.run(main())

print ('Adios')


