#!/bin/python3

"""

   Deezer.
   Manejador deezloader.

   Separa el robot de deezloader.
   Convierte respuestas de deezer en respuestas para el robot.

   Javier Martinez,  Agosto 2022

"""


class Conversor_deezer:
    
    

    """
    @param respuesta: resultado de una búsqueda de deezer
    """
    def convertir_busqueda (self, respuestas = [], consulta=None, tipo = 'track'):
        respuestas ['consulta'] = consulta #.partition ('&index=')[0] # PUEDE SER None
        respuestas ['tipo']     = tipo
        if 'next' in respuestas.keys ():
            respuestas ['siguiente']  = respuestas ['next'].rpartition ('index=')[2]
        if 'prev' in respuestas.keys ():
            respuestas ['anterior']  = respuestas ['prev'].rpartition ('index=') [2]

        for respuesta in respuestas ['data']:
            
            respuesta ['tipo']     = respuesta['type']
            respuesta ['enlace']   = respuesta['link']             
            

            if  respuesta ['tipo'] == 'track':
                respuesta ['texto']  = respuesta['artist']['name'] + ' - ' + respuesta['title']                
                next

            # cuando es una lista de albumes no tiene artista, tiene artista en el detalle
            if  respuesta ['tipo'] == 'album':
                if hasattr (respuesta, 'artist'):
                    respuesta ['texto']  = respuesta['artist']['name'] + ' - ' + respuesta['title']
                else:
                    respuesta ['texto']  = respuesta['title']
                next

            if  respuesta ['tipo'] == 'artist':
                respuesta ['texto']  = respuesta['name']
                next

            if  respuesta ['tipo'] == 'playlist':
                respuesta ['texto']  = '[' + str (respuesta ['nb_tracks']) + '] ' + respuesta['title'] + ' (' + respuesta['user'] ['name'] + ')'
                next
                                    
        return respuestas
        




    def convertir_cancion (self, respuesta= [], completo=False):
        respuesta ['titulo']       = respuesta ['title']
        respuesta ['artista']      = respuesta ['artist']['name']
        respuesta ['artista_id']   = respuesta ['artist']['id']
        respuesta ['tipo']         = respuesta ['type']
        respuesta ['enlace']       = respuesta ['link']                        
        respuesta ['duracion']     = respuesta ['duration']
        respuesta ['puntos']       = respuesta ['rank']
        respuesta ['demo']         = respuesta ['preview']                                
        respuesta ['fecha']        = respuesta ['release_date']
        respuesta ['album_titulo'] = respuesta ['album']['title'] 
        respuesta ['album_imagen'] = respuesta ['album']['cover_xl'] 
        respuesta ['album_enlace'] = respuesta ['album']['link'] 
        respuesta ['album_id']     = respuesta ['album']['id'] 

        respuesta ['artistas'] = []
        for artista in respuesta['contributors']:
            a = {}
            a ['id'] = artista ['id']
            a ['artista'] = artista ['name']
            a ['enlace'] = artista ['link']
            a ['imagen'] = artista ['picture_xl']            
            a ['tipo'] = artista ['type']

            respuesta ['artistas'] . append (a)

        return respuesta
 
 
        
    def convertir_artista (self, respuesta = []):        
        respuesta ['artista'] = respuesta ['name']
        respuesta ['enlace']  = respuesta ['link']
        respuesta ['imagen']  = respuesta ['picture_xl']        
        return respuesta

    

    def convertir_album (self, respuesta = []):        
        respuesta ['titulo']    = respuesta ['title']
        respuesta ['imagen']    = respuesta ['cover_xl']
        respuesta ['id_genero'] = respuesta ['genre_id']
        respuesta ['etiqueta']  = respuesta ['label']
        respuesta ['total']     = respuesta ['nb_tracks']
        respuesta ['duracion']  = respuesta ['duration']
        respuesta ['fecha']     = respuesta ['release_date']
        respuesta ['enlace']     = respuesta ['link']

        respuesta ['generos'] = []
        for elemento in respuesta['genres']['data']:
            nuevo_elemento            = {}
            nuevo_elemento ['id']     = elemento ['id']
            nuevo_elemento ['genero'] = elemento ['name']
            nuevo_elemento ['imagen'] = elemento ['picture']            
            nuevo_elemento ['tipo']   = elemento ['type']

            respuesta ['generos'] . append (nuevo_elemento)


        respuesta ['canciones'] = []
        for elemento in respuesta['tracks']['data']:
            nuevo_elemento = {}            

            nuevo_elemento ['id']           = elemento ['id']
            nuevo_elemento ['titulo']       = elemento ['title']
            nuevo_elemento ['artista']      = elemento ['artist']['name']
            nuevo_elemento ['artista_id']   = elemento ['artist']['id']
            nuevo_elemento ['tipo']         = elemento ['type']
            nuevo_elemento ['enlace']       = elemento ['link']                        
            nuevo_elemento ['duracion']     = elemento ['duration']
            nuevo_elemento ['puntos']       = elemento ['rank']
            nuevo_elemento ['demo']         = elemento ['preview']                                     
            

            respuesta ['canciones'] . append (nuevo_elemento)

        return respuesta


    def convertir_lista (self, respuesta = []):  
        
        respuesta ['titulo']         = respuesta ['title']
        respuesta ['total']          = respuesta ['nb_tracks']
        respuesta ['enlace']         = respuesta ['link']
        respuesta ['imagen']         = respuesta ['picture_xl']
        respuesta ['fecha']          = respuesta ['creation_date']        
        respuesta ['artista']        = respuesta ['creator'] ['name']
        respuesta ['artista_id']     = respuesta ['creator'] ['id']
        respuesta ['artista_enlace'] = respuesta ['creator'] ['tracklist']
        respuesta ['tipo']           = respuesta ['type']

        respuesta ['canciones'] = []
        for elemento in respuesta['tracks']['data']:
            nuevo_elemento = {}            

            nuevo_elemento ['id']           = elemento ['id']
            nuevo_elemento ['titulo']       = elemento ['title']
            nuevo_elemento ['artista']      = elemento ['artist']['name']
            nuevo_elemento ['artista_id']   = elemento ['artist']['id']
            nuevo_elemento ['tipo']         = elemento ['type']
            nuevo_elemento ['enlace']       = elemento ['link']                        
            nuevo_elemento ['duracion']     = elemento ['duration']
            nuevo_elemento ['puntos']       = elemento ['rank']
            nuevo_elemento ['demo']         = elemento ['preview']                                     
            

            respuesta ['canciones'] . append (nuevo_elemento)
            
        return respuesta




        

#     def tipo (self, respuesta):
# 		  return respuesta['type']
# 
# 
#     def nombre_artista (self, respuesta):		
# 		return respuesta['artist']['name']
# 
# 	def nombre_cancion (self, respuesta):
# 		return respuesta['title']
# 
# 	def nombre_album (self, respuesta):		
# 		return respuesta['album']['title']
# 
#     def duracion (self, respuesta):		
# 		return respuesta['duration']
# 		
#     def artista_cancion (self, respuesta):
#         return self.nombre_album (elemento) + ' - ' + self.nombre_cancion (elemento)
# 
#     def enlace (self, respuesta):		
# 		return respuesta['link']
# 	
#     def simplificar_lista (self, lista):
#         respuesta = []
#         for elemento in lista :
#             elemento = self.simplificar (elemento)
#             respuesta.append (elemento)
# 		return respuesta