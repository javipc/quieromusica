#!/bin/python3

"""

   Deezer.
   Adaptador para deezeriud.

   Instancia entre el robot Telegram y la API deezer.

   Javier Martinez,  Agosto 2022

"""

from recursos.opciones import opciones
# import deezer
# from deezloader.deezloader import DeeLogin
# from deezloader.deezloader import API
import deezeridu

from musica.conversor_api_deezer import Conversor_deezer

from recursos.msj import *


# def deezer_obtener_cliente (nombre_archivo_configuracion = 'configuracion/sesion.ini'):
#     configuracion_sesion = opciones (nombre_archivo_configuracion, 'deezer')
# 
#     application_id = configuracion_sesion.obtener ('application_id', '')
#     secret_key = configuracion_sesion.obtener ('secret_key', None)
#     
#     if secret_key is not None:
#         return deezer.Client(secret_key)
# 
#     return deezer.Client()
# 



class Deezer ():
	
	
	def __init__ (self, nombre_archivo_configuracion = 'configuracion/deezer.ini'):
		configuracion_sesion = opciones (nombre_archivo_configuracion, 'deezer')

		arl = configuracion_sesion.obtener ('arl', None)
		

				
		self.conversor_api = Conversor_deezer ()
		self.api = deezeridu.API()
		
		
		if arl is None:
			msj ("Error: falta la clave de deezer, no se podrá hacer descargas.")
			return
			
		
		
		msj ('Conectando deezer: ')		
		try:
			self.cliente_descarga = deezeridu.Deezer (arl)		
		except Exception as e:
			msj ('** Error al conectar con deezer ')
			if hasattr(e, 'message'):
				registrar(e.message)
			else:
				registrar(e)
			self.cliente_descarga = None
			return			


		msj ('API Lista')

		


	
	"""
	Uno interno:
	@param texto: un texto para buscar enlaces
	return lista: identificador , tipo (tema, album, lista), servicio (deezer)

	"""
		

	def obtener_info_enlace (self, texto, tipo= None):		
		texto=texto.strip().lower().strip ('/')
		if texto.isalnum():
			return None
		palabras = texto.split ()
		if len (palabras) == 0:
			return None
		palabra = palabras [-1]
		arreglo = palabra.split ('/')
		arreglo = arreglo [-3:]

		if len (arreglo) < 3 :
			return None
					
		if tipo is not None:
			if tipo != arreglo[-2]:
				return None	
		
		respuesta = {}
		respuesta['id']      = arreglo[-1]
		respuesta['tipo']    = arreglo[-2]
		respuesta['servicio']= arreglo[-3]
		return respuesta

	
	def a_enlace (self, identificador,tipo= 'track'):
		return 'https://www.deezer.com/' + tipo + '/' + identificador.strip()
		 

	def a_enlaces (self, identificadores,tipo= 'track'):
		respuesta = []
		for identificador in identificadores:
			respuesta.append (self.a_enlace (identificador, tipo))
		return respuesta
		


	"""
	Obtiene el último valor de un texto que se asemeje a un identificador.
	@param texto: es un enlace o un código
	"""
	def obtener_identificador (self, texto):
		texto=texto.strip().lower().strip ('/')
		
		if texto.isalnum():
			return texto
		
		if texto.replace(':', '').isalnum():
			return texto

		palabras = texto.split ()
		if len (palabras) == 0:
			return None
		
		palabra = palabras [-1]		
		arreglo = palabra.split ('/')		
		if arreglo[-1].isalnum():
			return arreglo[-1]

		return None
		


	"""
	Obtiene resultados.
	Puede ser búsquedas de texto.	
	@param consulta: un texto de búsqueda o un identificador, dependerá del tipo de dato a obtener.
	@param tipo: un tipo personalizado de búsqueda (cancion, album, etc)
	@returns: lista
	"""

	def buscar (self, consulta, tipo = None, indice = 0):
		msj ('Buscando: ', consulta + '&index=' + str (indice))
		respuesta = None
		try:
			respuesta = None
			
			if tipo == 'album':
				respuesta = self.api.search_album (consulta + '&index=' + str (indice))
			if tipo == 'playlist':
				respuesta = self.api.search_playlist (consulta + '&index=' + str (indice))				
			if tipo == 'track' :
				respuesta = self.api.search_track (consulta + '&index=' + str (indice))				
			if tipo == 'artist':
				respuesta = self.api.search_artist (consulta + '&index=' + str (indice))
			if tipo is None:
				respuesta = self.api.search (consulta + '&index=' + str (indice))
				tipo = 'track'
				
				
					
		except:
			msj ('Error buscando: ', consulta)
			return None
		
		if respuesta is None:
			msj ('Sin resultados de la búsqueda <', consulta, '>')
			return None

		# if 'data' in respuesta.keys():
		# 	respuesta = respuesta ['data']
		
		# se debe preparar para la búsqueda de enlaces
		respuesta = self.conversor_api.convertir_busqueda (respuesta, consulta, tipo)
		msj ("Respuesta")
		msj (respuesta)				
		
		return respuesta







	
	
	"""
	Obtiene detalles de contenidos específicos.	
	@param identificador: el id del contenido
	@param tipo: tipo de contenido (album, artista, tema)
	@returns: lista
	"""
	def obtener_detalle (self, identificador, tipo= 'track'):
		msj ('Obtener detalle : ', identificador, tipo)
		respuesta = None

		
		try:
			if tipo == 'track':
				respuesta = self.api.get_track (identificador)				
			if tipo == 'album':
				respuesta = self.api.get_album (identificador)				
			if tipo == 'artist':
				respuesta = self.api.get_artist (identificador)				
			if tipo == 'playlist':								
				respuesta = self.api.get_playlist (identificador)				

			if tipo == 'albumes':
				respuesta = self.api.get_artist_top_albums (identificador)
				# respuesta = respuesta ['data']
			if tipo == 'canciones':
				respuesta = self.api.get_artist_top_tracks (identificador)
				# respuesta = respuesta ['data']
			if tipo == 'listas':
				respuesta = self.api.get_artist_top_playlists (identificador)
				# respuesta = respuesta ['data']

		except Exception as e:
			msj ('error con el detalle: ', identificador, tipo)
			if hasattr(e, 'message'):
				registrar(e.message)
			else:
				registrar(e)
			return None

		if respuesta is None:
			msj ('No hay detalle : ', identificador, tipo)
			return None

		if tipo == 'track':			
			respuesta = self.conversor_api.convertir_cancion (respuesta)
		if tipo == 'album':			
			respuesta = self.conversor_api.convertir_album (respuesta)
		if tipo == 'artist':			
			respuesta = self.conversor_api.convertir_artista (respuesta)
		if tipo == 'playlist':											
			respuesta = self.conversor_api.convertir_lista (respuesta)

		if tipo == 'albumes':			
			respuesta = self.conversor_api.convertir_busqueda (respuesta)
		if tipo == 'canciones':			
			respuesta = self.conversor_api.convertir_busqueda (respuesta)
		if tipo == 'listas':			
			respuesta = self.conversor_api.convertir_busqueda (respuesta)

		
		msj ("Respuesta")
		msj (respuesta)

		return respuesta

		

		




	# 
	# if method_save == 0:
	# 	zip_name = f"{album}"
	# 
	# elif method_save == 1:
	# 	artist = __var_excape(song_metadata['ar_album'])
	# 	zip_name = f"{album} - {artist}"
	# 
	# elif method_save == 2:
	# 	artist = __var_excape(song_metadata['ar_album'])
	# 	upc = song_metadata['upc']
	# 	zip_name = f"{album} - {artist} {upc}"

	"""
	@param url: dirección url del archivo a descargar.
	@param calidad: [FLAC, MP3_128, MP3_320] .
	@param directorio: ruta de descargas.
	@returns: lista con datos del archivo descargado
	"""

	def descargar_deezer (self, url, calidad = 'FLAC', directorio = 'descargas'):
		msj ("Descargando DEEZER" , url, 'calidad', calidad, 'directorio', directorio)
		tema = None
		try:
			tema = self.cliente_descarga.download_track (
						url,
						output_dir         = directorio,
						quality_download   = calidad.upper(),
						recursive_quality  = True,
						recursive_download = True,
						method_save        = 1
					)
		

		except Exception as e:			
			if hasattr(e, 'message'):
				print (e.message)
			else:
				print (e)
			registrar ("DEEZER: Error descargando" , url, 'calidad', calidad, 'directorio', directorio)			
			return None


		
		if tema is None:
			return None
		respuesta = {}
		
		# datos que devuelve para que pueda ser usado por el robot
		respuesta ['calidad']      = tema.quality
		respuesta ['archivo']      = tema.song_path
		respuesta ['exitoso']      = tema.success
		respuesta ['nuevo_nombre'] = tema.artist + ' - ' + tema.music + tema.file_format
		respuesta ['artista']      = tema.artist
		respuesta ['cancion']      = tema.music
		respuesta ['genero']       = tema.genre
		respuesta ['id']           = tema.ids
		respuesta ['enlace']       = tema.link
		respuesta ['tema']         = tema

		if respuesta ['calidad'] .isnumeric ():
			if tema.file_format.lower () == '.mp3':
				respuesta ['calidad']='mp3_' + respuesta ['calidad']
		return respuesta

	def descargar_spotify (self, url, calidad = 'FLAC', directorio = 'descargas'):
		tema = self.cliente_descarga.download_track(
				url,
				output_dir = output_songs,
				quality_download = self.__quality,
				recursive_quality = recursive_quality,
				recursive_download = recursive_download,
				method_save = method_save,
				is_thread = is_thread
			)

		return tema


	def descargar (self, url, calidad = 'flac', directorio = 'descargas'):
		msj ("Descargando" , url, 'calidad', calidad, 'directorio', directorio)
		tema = None
		try:
			if "deezer" in url:
				tema = self.descargar_deezer (url, calidad = calidad.upper(), directorio = directorio)

			elif "spotify" in url:
				tema = self.descargar_spotify (url, calidad = calidad, directorio = directorio)
				
		except TrackNotFound as error:
			registrar (error)

			

		return tema
			


	# https://api.deezer.com/search/?q=marga%20sol&index=25
	# https://api.deezer.com/search/artist/?q=marga&index=25
	def obtener_pagina (self, enlace = 'https://api.deezer.com/search/?q=marga%20sol&index=25'):
		respuesta = deezeridu.api.req_get(enlace)
		respuesta.close ()
		if respuesta.ok:
			return self.conversor_api.convertir_busqueda (respuesta.json ())
		return None


