# Quiero Música
RoBOT de música... Sí, otro más.


## ESTÁ EN DESARROLLO:
Todavía no se asoma a la etapa de pruebas, está en desarrollo pero comparto el código a quién le interese.

https://t.me/QuieroMusicaBot estará en línea cuando consiga una computadora para usar de servidor.
Por ahora está en un teléfono para la etapa de prueba (las descargas son lentas).

Por favor, solo música, es decir: ¡NADA DE CUMBIANCHA NI REGUETÓN!

## Características:
* Capacidad de reanudar descargas en caso de reinicios.
* Reinicia automáticamente en caso de pérdida de conexión (pyrogram no recupera la conexión).
* Elección de base de datos MySQL/MariaDB o SQLite3.
* Búsqueda por texto o comandos: /buscar artista Gustavo Cerati.

## Requiere:
* Api ID y HASH de Telegram: https://core.telegram.org/api/obtaining_api_id
* Robot de Telegram: pedír prestado a https://t.me/BotFather
* Preferentemente una cuenta deezer: https://www.deezer.com/es/ para descargar temas, sin cuenta debería funcionar igual pero no se va a poder descargar.


## Sistema:
* python3: El lenguaje.
* pip3: para instalar dependencias.
* mysql-connector-python: Para la base de datos.
* pyrogram: El robot.
* tgcrypto: Para que funcione pyrogram.
* deezeridu: Para comunicarse con la API de Deezer.
* apscheduler: Para enviar mensajes al inicio del programa.


## Contenedor:
En el directorio "contenedor" hay un archivo para crear una imagen con los requisitos básicos.
El archivo .sh es para crear una cápsula con contenedores podman, uno para la aplicación y otro para la base de datos.
El proyecto ya está clonado en la imagen pero si querés editarlo deberás adaptar el directorio para que apunte a tu directorio de desarrollo.


## Créditos:
* https://github.com/OpenJarbas/deezeridu
* https://pyrogram.org/

## Contacto
Consultas, elogios, puteadas: http://javicel.t.me/

## ¿Cómo colaborar?:
https://gitlab.com/javipc/mas

Gracias por la visita 👋🏿
